# PyOWscaling Changelog

## [Unreleased]

### Added

### Changed
- Transfer of the repository to https://codeberg.org/SMP/PyOWscaling
- Removed 'master' branch to 'main'.

### Deprecated

### Removed

### Fixed

### Security


## [3.1.1] - 2017-09-19

### Changed
- Improvements:
    - Added forgotten copyright/sources to new stripe method.

## [3.1.0] - 2017-09-19

### Changed
- Scaling methods:
    - Added a native version of the stripe (aka strip) scaling method.
- Friction lines:
    - Added friction line suggested by CEHIPAR.
    - Added Prandtl's friction line (needed by CEHIPAR).
    - Added Blasius's friction line (needed by CEHIPAR).
- API:
    - `self.KT_ow` and `self.KQ_ow` are generally set in the `IScalingPlugin`,
      so that there is no need to do it in every plug-in implementation.
- Bugs:
    - Corrected `endpoints` default value in `owscaling.helpers.LimitedInt`.
- Improvements:
    - Added `owscaling.helpers.Spline` class (and used it in the geometry
      reader plug-ins).
    - Added exceptions to docstrings.
    - General improvements of docstrings.

## [3.0.0] - 2017-08-08

### Changed
- Major rewrite (and breaking) of the initialization API.

## [2.0.0] - 2017-07-07

### Added
- [Documentation](https://codeberg.org/SMP/PyOWscaling/wiki/home) started by @sphh.
- Property ``__filename__`` containing the file name of the implemented plug-in added by @sphh.

### Changed
- Composite viscosity and density changed to single viscosity and density by @sphh:
    - Method signature `IScalingPlugin.initialize()`` changed.
    - Command line options for ``openwater.py`` changed: Options ``--viscosity-ow``, ``--viscosity-sp`` and ``--viscosity-fs`` added and ``--viscosity`` removed.
    - Functions signature ``OWScaling.__init__()`` and ``OWScaling.initialize()`` changed.
    - Properties ``OWScaling.viscosity`` and ``OWScaling.density`` removed and ``OWScaling.viscosity_ow``, ``OWScaling.viscosity_sp``, ``OWScaling.viscosity_fs``, ``OWScaling.density_ow``, ``OWScaling.density_sp`` and ``OWScaling.density_fs`` added.
- Added keyword arguments to ``IPropellergeometryPlugin.fit()`` and ``IOWReaderPlugin.fit()`` by @sphh.
- ``f`` parameter in ``_IFile.get_filename()`` and ``_IFile.get_fileencoding()`` is made optional @sphh.
- Formatting of debugging output from ``openwater.py`` improved by @sphh.
- Source code documentation improved by @sphh.
- Source code cleaned up by @sphh.

### Removed
- Method ``initialization()`` removed from all plug-ins by @sphh.
- Method ``_finalize()`` removed from all plug-ins by @sphh.


## [1.0.0] - 2017-06-09

Initial commit.
