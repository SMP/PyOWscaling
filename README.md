# PyOWscaling

## Objectives

A framework to scale propeller characteristics (open-water curves) measured at model scale to different Reynolds number regimes. This is required for the the power prediction of the trials when model testing ships.

The aim was not to write the fastest code, but to create a framework, which is easily extendable and lends itself to experimentation with different aspects of the scaling methods. That does not mean that it cannot be used for “production”.


## Documentation

The documentation can be found in the [PyOWscaling wiki](https://codeberg.org/SMP/PyOWscaling/wiki/home).


## License

The software is released under the [GNU Lesser General Public License](https://choosealicense.com/licenses/lgpl-3.0/).


## Bug report

Please report bugs at [PyOWscaling/issues](https://codeberg.org/SMP/PyOWscaling/issues).


## Downloads

See [Releases](https://codeberg.org/SMP/PyOWscaling/releases).


## Changelog

See [Changelog](CHANGELOG.md).
