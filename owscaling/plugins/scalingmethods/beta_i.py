#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `BetaI` implementing the `IScalingPlugin` interface.

Plug-ins:
    BetaI:
        The βᵢ scaling method.


Created on Fri Jan 27 10:11:39 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

import argparse

from numpy import pi, sqrt, sin, cos, arctan, mod
import scipy.integrate

from owscaling.plugins import IScalingPlugin, autoargs


pi_sq = pi*pi


class BetaI(IScalingPlugin):
    """The βᵢ scaling method according to S. Helma.

    Source:
        Helma, S.:
            ‘An Extrapolation Method Suitable for Scaling of Propellers of
            any Design’.
            The Fourth International Symposium on Marine Propulsors (smp’15),
            2015, Austin, USA.
        Helma, S.:
            ‘A scaling procedure for modern propeller designs’.
            Ocean Engineering 2016, 120, pp. 165-174.
    """
    name = 'beta_i'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            alternative_formulation: {
                'args': '--alternative',
                'action': 'store_true',
                'help': 'use alternative formulation to calculated beta_i'
                } = False,
            betai: {
                'args': '--betai',
                'type': argparse.FileType('w'),
                'help': 'name of output file for βᵢ values',
                'metavar': 'FILE'
                } = None,
            *args, **kwargs):
        super().arguments(*args, **kwargs)


    def _scale(self):
        """Scale the open-water data."""

        # Note:
        #   The formulae follow [Helma 2016].

        #
        # Some shortcuts
        #

        p = self.propeller      # Propeller
        ow = self.openwater     # Open-water data

        XE = self.args.XE       # Fractional radius of significant profile

        # Propeller diameters ...
        DP_ow = self.DP['ow']   # ... open-water
        DP_sp = self.DP['sp']   # ... self-propulsion
        DP_fs = self.DP['fs']   # ... full scale

        # Propeller shaft revs ...
        N_ow = self.N['ow']     # ... open-water
        N_sp = self.N['sp']     # ... self-propulsion
        N_fs = self.N['fs']     # ... full scale

        #
        # Store some common values for open-water, self-propulsion and full
        # scale
        #

        # Chord lengths of significant profile
        LCH_ow = p.chr(XE) * DP_ow/2
        LCH_sp = p.chr(XE) * DP_sp/2
        LCH_fs = p.chr(XE) * DP_fs/2

        # Kinematic viscosities of tank and sea water
        VK_ow = self.viscosity['ow'](self.TEWA['ow'])
        VK_sp = self.viscosity['sp'](self.TEWA['sp'])
        VK_fs = self.viscosity['fs'](self.TEWA['fs'])

        #
        # Shortcut to functions of JEI
        #

        # Speeds of attack
        V_ow = lambda JEI: N_ow(JEI) * DP_ow * sqrt(JEI**2 + (pi*XE)**2)
        V_sp = lambda JEI: N_sp * DP_sp * sqrt(JEI**2 + (pi*XE)**2)
        V_fs = lambda JEI: N_fs * DP_fs * sqrt(JEI**2 + (pi*XE)**2)

        # Reynolds numbers
        RN_ow = lambda JEI: V_ow(JEI) * LCH_ow / VK_ow
        RN_sp = lambda JEI: V_sp(JEI) * LCH_sp / VK_sp
        RN_fs = lambda JEI: V_fs(JEI) * LCH_fs / VK_fs

        ##
        ## Do the scaling
        ##

        #
        # Geometries
        #

        # Geometric inflow angle (a function of J!)
        beta = lambda JEI: arctan(JEI / (pi * XE))                       # (4g)

        # Geometric constants
        if not self.args.alternative_formulation:
            kappa_T = pi_sq * p.NPB / 8  * p.chr(XE) * (1-p.XBDR**3)/3   # (4c)
            kappa_Q = pi_sq * p.NPB / 16 * p.chr(XE) * (1-p.XBDR**4)/4   # (4d)
            kappa = 3/8 * (1-p.XBDR**4) / (1-p.XBDR**3)                  # (4e)
        else:
            gamma_T_ = lambda JEI: scipy.integrate.quad(
                    lambda x, JEI: p.chr(x) * x * x / cos(beta(JEI))**2,
                    p.XBDR, 1, args=(JEI,))[0]
            gamma_Q_ = lambda JEI: scipy.integrate.quad(
                    lambda x, JEI: p.chr(x) * x * x * x / cos(beta(JEI))**2,
                    p.XBDR, 1, args=(JEI,))[0]
            gamma_T = lambda JEI: pi_sq * p.NPB / 8 * gamma_T_(JEI)    # (A.3c)
            gamma_Q = lambda JEI: pi_sq * p.NPB / 16 * gamma_Q_(JEI)   # (A.3d)
            gamma = lambda JEI: gamma_Q_(JEI) / gamma_T_(JEI) / 2      # (A.3e)


        #
        # Analyse the open-water tests
        #

        # Thrust and torque coefficients (functions of J!)
        KT_ow = self.KT_ow
        KQ_ow = self.KQ_ow

        dKT = KT_ow.deriv()
        dKQ = KQ_ow.deriv()

        # Hydrodynamic inflow angle βᵢ and lift and drag coefficients of
        # significant profile (functions of J!)
        if not self.args.alternative_formulation:
            beta_i_ow = lambda JEI: mod(
                    arctan(-kappa * dKT(JEI) / dKQ(JEI)),               # (22a)
                    pi/2)
            B_ow = lambda JEI: (                                         # (4f)
                    cos(beta_i_ow(JEI)-beta(JEI)) / cos(beta(JEI)))

            CL_ow = lambda JEI: (                                       # (17a)
                KQ_ow(JEI)/kappa_Q * sin(beta_i_ow(JEI))
                + KT_ow(JEI)/kappa_T * cos(beta_i_ow(JEI))) / B_ow(JEI)**2
            CD_ow = lambda JEI: (                                       # (17b)
                KQ_ow(JEI)/kappa_Q * cos(beta_i_ow(JEI))
                - KT_ow(JEI)/kappa_T * sin(beta_i_ow(JEI))) / B_ow(JEI)**2
        else:
            beta_i_ow = lambda JEI: mod(
                    arctan(-gamma(JEI) * dKT(JEI) / dKQ(JEI)),          # (22a)
                    pi/2)

            CL_ow = lambda JEI: (                                       # (17a)
                KQ_ow(JEI)/gamma_Q(JEI) * sin(beta_i_ow(JEI))
                + KT_ow(JEI)/gamma_T(JEI) * cos(beta_i_ow(JEI)))
            CD_ow = lambda JEI: (                                       # (17b)
                KQ_ow(JEI)/gamma_Q(JEI) * cos(beta_i_ow(JEI))
                - KT_ow(JEI)/gamma_T(JEI) * sin(beta_i_ow(JEI)))

        # Frictional drag coefficient of significant profile (function of J!)
        CF_ow = lambda JEI: self.CF('ow', RN_ow(JEI), XE, DP_ow, JEI=JEI)


        # Form drag coefficient (three and two dimensional) (function of J!).
        # Please note: The HSVA 'Relevant Geometrical Data' file does not
        # contain the thickness distribution, hence we cannot calculate the
        # section form drag CD_2d from eq. (25). We get around it by combining
        # the 2d and 3d form drag coefficients CD_2d and CD_3d, respectively.
        # We can get away with that approach, because neither is subject to
        # any scaling effects.
        CD_3d_x_CD_2d_ow = lambda JEI: CD_ow(JEI) / (2 * CF_ow(JEI))     # (28)


        #
        # Scale the open-water test to self-propulsion condition
        #

        if not self.scale_to['sp']:
            KT_sp = KT_ow
            KQ_sp = KQ_ow
        else:
            # Drag coefficients of significant profile (functions of J!) ...

            # ... frictional drag coefficient
            CF_sp = lambda JEI: self.CF('sp', RN_sp(JEI), XE, DP_sp, JEI=JEI)

            # ... form drag coefficient  (3d & 2d)
            CD_3d_x_CD_2d_sp = CD_3d_x_CD_2d_ow

            # ... lift and drag coefficients
            CL_sp = CL_ow
            CD_sp = lambda JEI: 2 * CD_3d_x_CD_2d_sp(JEI) * CF_sp(JEI)   # (28)

            # Hydrodynamic inflow angle (a function of J!)
            beta_i_sp = beta_i_ow

            # Thrust and torque coefficients (functions of J!)
            if not self.args.alternative_formulation:
                B_sp = B_ow
                KT_sp = lambda JEI: kappa_T * B_sp(JEI)**2 * (           # (4a)
                    CL_sp(JEI)*cos(beta_i_sp(JEI))
                    - CD_sp(JEI)*sin(beta_i_sp(JEI)))
                KQ_sp = lambda JEI: kappa_Q * B_sp(JEI)**2 * (           # (4b)
                    CL_sp(JEI)*sin(beta_i_sp(JEI))
                    + CD_sp(JEI)*cos(beta_i_sp(JEI)))
            else:
                KT_sp = lambda JEI: gamma_T(JEI) * (                     # (4a)
                    CL_sp(JEI)*cos(beta_i_sp(JEI))
                    - CD_sp(JEI)*sin(beta_i_sp(JEI)))
                KQ_sp = lambda JEI: gamma_Q(JEI) * (                     # (4b)
                    CL_sp(JEI)*sin(beta_i_sp(JEI))
                    + CD_sp(JEI)*cos(beta_i_sp(JEI)))


        #
        # Scale the open-water test to full scale condition
        #

        if not self.scale_to['fs']:
            KT_fs = KT_ow
            KQ_fs = KQ_ow
        else:
            # Drag coefficients of significant profile (functions of J!) ...

            # ... frictional drag coefficient
            CF_fs = lambda JEI: self.CF('fs', RN_fs(JEI), XE, DP_fs, JEI=JEI)

            # ... form drag coefficient (3d & 2d)
            CD_3d_x_CD_2d_fs = CD_3d_x_CD_2d_ow

            # ... lift and drag coefficients
            CL_fs = CL_ow
            CD_fs = lambda JEI: 2 * CD_3d_x_CD_2d_fs(JEI) * CF_fs(JEI)   # (28)


            # Hydrodynamic inflow angle (a function of J!)
            beta_i_fs = beta_i_ow

            # Thrust and torque coefficients (functions of J!)
            if not self.args.alternative_formulation:
                B_fs = B_ow
                KT_fs = lambda JEI: kappa_T * B_fs(JEI)**2 * (           # (4a)
                    CL_fs(JEI)*cos(beta_i_fs(JEI))
                    - CD_fs(JEI)*sin(beta_i_fs(JEI)))
                KQ_fs = lambda JEI: kappa_Q * B_fs(JEI)**2 * (           # (4b)
                    CL_fs(JEI)*sin(beta_i_fs(JEI))
                    + CD_fs(JEI)*cos(beta_i_fs(JEI)))
            else:
                KT_fs = lambda JEI: gamma_T(JEI) * (                     # (4a)
                    CL_fs(JEI)*cos(beta_i_fs(JEI))
                    - CD_fs(JEI)*sin(beta_i_fs(JEI)))
                KQ_fs = lambda JEI: gamma_Q(JEI) * (                     # (4b)
                    CL_fs(JEI)*sin(beta_i_fs(JEI))
                    + CD_fs(JEI)*cos(beta_i_fs(JEI)))



        # Store the scaled KT and KQ curves
        self.KT_sp = KT_sp
        self.KQ_sp = KQ_sp
        self.KT_fs = KT_fs
        self.KQ_fs = KQ_fs


        # Save the βᵢ(J) distribution (and some more information) to a file
        if self.args.betai:
            with self.args.betai as f:
                # Reynolds number
                print('# JEI beta beta_i CD_form RN', file=f)
                # JEI, beta_i
                for JEI in ow.JEI_data:
                    print(JEI,
                          beta(JEI), beta_i_ow(JEI),
                          CD_3d_x_CD_2d_ow(JEI), RN_ow(JEI),
                          file=f)
