#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `Stripe` implementing the `IScalingPlugin` interface.

Plug-ins:
    Stripe:
        The stripe (aka strip) method for propeller scaling.


Created on Fri Aug 11 11:09:14 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from numpy import pi, sqrt, sin, cos, arctan2
import scipy.integrate

from owscaling.plugins import IScalingPlugin, autoargs


pi2 = pi*pi


class Stripe(IScalingPlugin):
    """The stripe (aka strip) method for propeller scaling.

    Why is it called "stripe" and not "strip"? A stripe is "a long narrow band
    or strip, typically of the same width throughout its length, differing in
    color or texture from the surface on either side of it", and a strip is a
    "a long, narrow piece of cloth, paper, plastic, or some other material".
    (Reference: Strip, Stripe. 2013. Oxford Dictionaries. Oxford University
    Press. 27 January, 2014, cited in
    https://english.stackexchange.com/questions/148261/difference-between-nouns-strip-and-stripe#answer-148266)

    There are a couple of mentioning of this method in the literature, dating
    back to 1994 (see Souces: below). The idea to align the lift force with
    the inflow angle β instead of the pitch angle φ was thought the first time
    by Stephan Helma in spring 2017 and implemented here for the first time.

    Source:
        Praefke, E. (1994):
            ‘Multi-Component Propulsors for Merchant Ships – Design
            Considerations and Model Test Results’.
            SNAME Symposium “Propeller/Shafting’94”, Virginia, USA.
        Streckwall, H., Greitsch, L., Müller, J., Scharf, M. & Bugalski, T. (2013):
            ‘Development of a Strip Method Proposed as New Standard for
            Propeller Performance Scaling’.
            Ship Technology Research, Vol. 60, pp. 58–69.
        Quereda, R., Soriano, C., Pérez-Sobrino, M., González-Adalid, J.,
        Morán, A. & Gennaro, G. (2016):
            ‘Scale effects in open water tests results for performance
            prediction of conventional and unconventional propellers’.
            Instituto Nacional de Técnica Aeroespacial, Canal de Experiencias
            Hidrodinámicas de El Padro, Publicacion núm. 215, Madrid,
            April 2016.
    """
    name = 'Stripe'
    __version__ = '3.1.1'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            betaalignment: {
                'args': ('-a', '--betaalignment'),
                'action': 'store_true',
                'help': 'align the section force components with geometric'
                        'inflow angle β instead of the pitch angle φ'
                } = False,
            warning: {
                'args': ('-w', '--nowarning'),
                'action': 'store_false',
                'help': 'do not issue a warning '
                        'if no rake information is available'
                } = True,
            *args, **kwargs):
        super().arguments(*args, **kwargs)


    def _scale(self):
        """Scale the open-water data."""

        #
        # Some shortcuts
        #

        p = self.propeller      # Propeller
        ow = self.openwater     # Open-water data

        NPB = p.NPB             # Number of propeller blades

        # Propeller diameters ...
        DP_ow = self.DP['ow']   # ... open-water
        DP_sp = self.DP['sp']   # ... self-propulsion
        DP_fs = self.DP['fs']   # ... full scale

        # Propeller shaft revs ...
        N_ow = self.N['ow']     # ... open-water
        N_sp = self.N['sp']     # ... self-propulsion
        N_fs = self.N['fs']     # ... full scale

        #
        # Store some common values for open-water, self-propulsion and full
        # scale
        #

        # Chord lengths of profile at x
        LCH_ow = lambda X: p.chr(X) * DP_ow/2
        LCH_sp = lambda X: p.chr(X) * DP_sp/2
        LCH_fs = lambda X: p.chr(X) * DP_fs/2

        # Chord to diameter ratio
        CDR = lambda X: p.chr(X) / 2

        # Kinematic viscosities of tank and sea water
        VK_ow = self.viscosity['ow'](self.TEWA['ow'])
        VK_sp = self.viscosity['sp'](self.TEWA['sp'])
        VK_fs = self.viscosity['fs'](self.TEWA['fs'])

        #
        # Shortcut to functions of JEI
        #

        # Speeds of attack of profile at x
        V_ow = lambda JEI, X: N_ow(JEI) * DP_ow * sqrt(JEI**2 + (pi*X)**2)
        V_sp = lambda JEI, X: N_sp * DP_sp * sqrt(JEI**2 + (pi*X)**2)
        V_fs = lambda JEI, X: N_fs * DP_fs * sqrt(JEI**2 + (pi*X)**2)

        # Reynolds numbers
        RN_ow = lambda JEI, X: V_ow(JEI, X) * LCH_ow(X) / VK_ow
        RN_sp = lambda JEI, X: V_sp(JEI, X) * LCH_sp(X) / VK_sp
        RN_fs = lambda JEI, X: V_fs(JEI, X) * LCH_fs(X) / VK_fs

        # Geometric inflow angle β or pitch angle φ?
        if self.args.betaalignment:
            # Geometric inflow angle β (a function of J!)
            beta_phi = lambda JEI, X: arctan2(JEI, pi * X)
        else:
            # Pitch angle φ
            beta_phi = lambda JEI, X: p.pdr(X)


        #
        # Do the scaling
        #

        # CD = 2 · CD2d · CF
        # ΔCD = CDm - CDs
        Delta_CD_sp = lambda JEI, X: (
                self.CD('ow', RN_ow(JEI, X), X, DP_ow, JEI=JEI) -
                self.CD('sp', RN_sp(JEI, X), X, DP_ow, JEI=JEI))
        Delta_CD_fs = lambda JEI, X: (
                self.CD('ow', RN_ow(JEI, X), X, DP_ow, JEI=JEI) -
                self.CD('fs', RN_fs(JEI, X), X, DP_fs, JEI=JEI))

        # Get the open-water test curves
        KT_ow = self.KT_ow
        KQ_ow = self.KQ_ow

        # Scale it:
        #   ΔKT = -Z/2  ∫ΔCD [J² + (πx)²] C/D sinβᵢ dx
        #   ΔKQ =  Z/4  ∫ΔCD [J² + (πx)²] C/D cosβᵢ x dx
        # These formulae are valid for propellers without rake. If the rake
        # should be included, dx has to replaced by the arc length ds(x):
        #   dx = ds(x) = d[∫√(1+f'²(x))dx] = (1+f'²(x))dx
        # with f..the rake of the propeller

        # ds(x) due to rake
        try:
            drkr = p.rkr.derivative()
            dcurvelength = lambda x: sqrt(1+drkr(x)**2)
        except AttributeError:
            if self.args.warning:
                import warnings
                warnings.warn(
                    'No rake information available from propeller geometry. '
                    'Proceeding with zero rake.')
            dcurvelength = lambda x: 1

        # Self-propulsion
        if self.scale_to['sp']:
            Delta_KT_sp = lambda JEI: -NPB/2 * scipy.integrate.quad(
                    lambda x, JEI: (
                            Delta_CD_sp(JEI, x)
                            * (JEI*JEI + pi2*x*x)
                            * CDR(x)
                            * sin(beta_phi(JEI, x))
                            * dcurvelength(x)),
                    p.XBDR, 1, args=(JEI,))[0]
            Delta_KQ_sp = lambda JEI: NPB/4 * scipy.integrate.quad(
                    lambda x, JEI: (
                            Delta_CD_sp(JEI, x)
                            * (JEI*JEI + pi2*x*x)
                            * CDR(x)
                            * cos(beta_phi(JEI, x))
                            * x
                            * dcurvelength(x)),
                    p.XBDR, 1, args=(JEI,))[0]
            # Store the scaled KT and KQ curves
            self.KT_sp = lambda JEI: KT_ow(JEI) - Delta_KT_sp(JEI)
            self.KQ_sp = lambda JEI: KQ_ow(JEI) - Delta_KQ_sp(JEI)
        else:
            # Store the unmodified KT and KQ curves
            self.KT_sp = KT_ow
            self.KQ_sp = KQ_ow

        # Full scale
        if self.scale_to['fs']:
            Delta_KT_fs = lambda JEI: -NPB/2 * scipy.integrate.quad(
                    lambda x, JEI: (
                            Delta_CD_fs(JEI, x)
                            * (JEI*JEI + pi2*x*x)
                            * CDR(x)
                            * sin(beta_phi(JEI, x))
                            * dcurvelength(x)),
                    p.XBDR, 1, args=(JEI,))[0]
            Delta_KQ_fs = lambda JEI: NPB/4 * scipy.integrate.quad(
                    lambda x, JEI: (
                            Delta_CD_fs(JEI, x)
                            * (JEI*JEI + pi2*x*x)
                            * CDR(x)
                            * cos(beta_phi(JEI, x))
                            * x
                            * dcurvelength(x)),
                    p.XBDR, 1, args=(JEI,))[0]
            # Store the scaled KT and KQ curves
            self.KT_fs = lambda JEI: KT_ow(JEI) - Delta_KT_fs(JEI)
            self.KQ_fs = lambda JEI: KQ_ow(JEI) - Delta_KQ_fs(JEI)
        else:
            # Store the unmodified KT and KQ curves
            self.KT_fs = KT_ow
            self.KQ_fs = KQ_ow
