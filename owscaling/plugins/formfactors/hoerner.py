#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-ins `HoernerXXX` implementing the `IFormfactorPlugin` interface.

Plug-ins:
    Hoerner:
        Form factor according to Hörner.
    Hoerner6465:
        Form factor according to Hörner for NACA 64 and 65 profiles.


Created on Sun Feb  5 21:39:21 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from owscaling.plugins import IFormfactorPlugin


class Hoerner(IFormfactorPlugin):
    """Form factor according to Hörner.

    Source:
        Hoerner, S.F.:
            Fluid-Dynamic Drag, theoretical, experimental and statistical
            information.
            Bakersfield, USA: Hoerner Fluid Dynamics, 1965, reprint.
    """
    name = 'Hoerner'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def __call__(self, XTC, **kwargs):
        """Calculate the form factor of a section profile with thickness `XTC`.

        Parameters:
            XTC:
                The thickness to chord length ratio [-].

        Return:
            The form factor [-].
        """

        return 1 + 2*XTC + 60*XTC**4


class Hoerner6465(IFormfactorPlugin):
    """Form factor according to Hörner for NACA 64 and 65 profiles.

    Source:
        Hoerner, S.F. (1965), reprint:
            Fluid-Dynamic Drag, theoretical, experimental and statistical
            information.
            Bakersfield, USA: Hoerner Fluid Dynamics.
    """
    name = 'Hoerner64/65'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def __call__(self, XTC, **kwargs):
        """Calculate the form factor of a section profile with thickness `XTC`.

        Parameters:
            XTC:
                The thickness to chord length ratio [-].

        Return:
            The form factor [-].
        """

        return 1 + 1.2*XTC + 70*XTC**4
