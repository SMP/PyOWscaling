#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `Constant` implementing the `IFormfactorPlugin` interface.

Plug-ins:
    Constant:
        Constant form factor.


Created on Sun Feb  5 21:34:58 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from owscaling.plugins import IFormfactorPlugin, autoargs
from owscaling.helpers import LimitedFloat


class Constant(IFormfactorPlugin):
    """Constant form factor."""
    name = 'Constant'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            const: {
                'type': LimitedFloat(min=0),
                'help': 'constant value for form factor'
                } = 1,
            *args, **kwargs):
        super().arguments(*args, **kwargs)


    def __str__(self):
        return (super().__str__() +
        """Const:      {self.args.const}
        """.format(self=self))

    def __call__(self, XTC, **kwargs):
        """Calculate the form factor of a section profile with thickness `XTC`.

        Parameters:
            XTC:
                The thickness to chord length ratio [-].

        Return:
            The form factor [-].
        """

        return self.args.const
