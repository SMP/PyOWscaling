#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-ins `TransitionFrictionXXX` implementing the `IFrictionlinePlugin`
interface.

Plug-ins:
    TransitionFrictionOW:
        Local friction coefficient as used by HSVA for the open-water
        condition with undisturbed inflow.
    TransitionFrictionBehind:
        Local friction coefficient as used by HSVA for the behind condition
        with disturbed inflow.


Created on Thu Mar  9 08:31:36 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from numpy import log10, exp

from owscaling.plugins import IFrictionlinePlugin, autoargs


class HSVAFrictionMixin(object):
    """Local friction coefficient as used by HSVA.

    Total friction coefficient cF
    =============================

    The total friction coefficient cF for the whole blade (both sides) and
    including the form factor is according to Streckwall/HSVA
        CD = a·[d/(1+r²) + (1-d)·exp(-r²/2)]
    where
        r = (log₁₀Rn - b)/c

    WARNING:
        This is for the whole blade (both lines) and includes the form factor!
        - Hence we use a/2 as first factor.
        - Use "--formfactor Constant 1" to not include any additional form
          factor in the calculation.
    """
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def __init__(self):

        self.a = self.args.a
        self.b = self.args.b
        self.c = self.args.c
        self.d = self.args.d

        # a/2
        self.a_2 = self.args.a / 2      # See WARNING in class's documentation.

    def __str__(self):
        return """a:          {self.args.a}
        b:          {self.args.b}
        c:          {self.args.c}
        d:          {self.args.d}
        """.format(self=self)

    def __call__(self, RN, **kwargs):
        """Calculate the friction coefficient of a section.


        WARNING:
            The friction coefficient given by Streckwall/HSVA is for the
            *whole* blade (both sides) and *includes* the form factor!
            - Hence we use a/2 as first factor.
            - Use "--formfactor Constant 1" to not include any additional
              form factor in the calculation.

        Parameters:
            RN:
                The Reynolds number based on the section chord length [-].

        Return:
            The frictional coefficient [-].
        """

        r = ((log10(RN) - self.args.b)/self.args.c)**2
        return self.a_2 * (self.d / (1+r) + (1-self.d) * exp(-r/2))


class HSVAFrictionOW(HSVAFrictionMixin, IFrictionlinePlugin):
    """Local friction coefficient as used by HSVA for the open-water
    condition with undisturbed inflow.
    """
    name = 'HSVA-OW'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            a: {
                'args': '--a',
                'type': float,
                'help': 'factor a for the friction coefficient'
                } = 0.02145435039201,
            b: {
                'args': '--b',
                'type': float,
                'help': 'factor b for the friction coefficient'
                } = 4.174741254548,
            c: {
                'args': '--c',
                'type': float,
                'help': 'factor c for the friction coefficient'
                } = 0.9112701673967,
            d: {
                'args': '--d',
                'type': float,
                'help': 'factor d for the friction coefficient'
                } = 3.029492755962,
            *args, **kwargs):
        IFrictionlinePlugin.arguments(self, *args, **kwargs)


    def __init__(self, *args, **kwargs):
        IFrictionlinePlugin.__init__(self, *args, **kwargs)
        HSVAFrictionMixin.__init__(self)

    def __str__(self):
        return IFrictionlinePlugin.__str__(self)+HSVAFrictionMixin.__str__(self)


class HSVAFrictionBehind(HSVAFrictionMixin, IFrictionlinePlugin):
    """Local friction coefficient as used by HSVA for the behind condition
    with disturbed inflow.
    """
    name = 'HSVA-Behind'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            a: {
                'args': '--a',
                'type': float,
                'help': 'factor a for the friction coefficient'
                } = 0.02546689917582,
            b: {
                'args': '--b',
                'type': float,
                'help': 'factor b for the friction coefficient'
                } = 3.980310869827,
            c: {
                'args': '--c',
                'type': float,
                'help': 'factor c for the friction coefficient'
                } = 0.9163209307599,
            d: {
                'args': '--d',
                'type': float,
                'help': 'factor d for the friction coefficient'
                } = 2.704789857162,
            *args, **kwargs):
        IFrictionlinePlugin.arguments(self, *args, **kwargs)


    def __init__(self, *args, **kwargs):
        IFrictionlinePlugin.__init__(self, *args, **kwargs)
        HSVAFrictionMixin.__init__(self)

    def __str__(self):
        return IFrictionlinePlugin.__str__(self)+HSVAFrictionMixin.__str__(self)


if __name__ == '__main__':

    fl = HSVAFrictionBehind({})
    print(fl)

    RN = 1e9            # Reynolds number [-]
#    XKPLCH = 0.00001    # Ratio roughness to section length [-]

#    LCH = fl.KP/XKPLCH  # Section length [m]

    f1 = '{:<6} {:<14} {:<14} {:<10} {:<10}'
    f = '{:<6.0e} {:<14.4} {:<14.4} {:<10.4} {:<10.4}'
    print('Friction coefficient for')
#    print('    LCH={}m'.format(LCH))
#    print('    k={:g}m'.format(fl.KP))
#    print('    k/LCH={:g}'.format(XKPLCH))
    print(f1.format('RN', 'ITTC1978 ship', 'ITTC1978 m', 'ITTC1957', 'FrictionOW'))
    for i in range(4,11):
        RN = 10**i
        print(f.format(
                RN,
                0., #(1.89 + 1.62*log10(LCH / fl.KP))**(-2.5),
                0.044/RN**(1/6) - 5/RN**(2/3),
                0.075 / (log10(RN)-2)**2,
                fl(RN)))
