#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-ins `PropellerDetailed` and `Openwater` implementing the
`IPropellergeometryPlugin` interface.

Plug-ins:
    PropellerDetailed:
        Read the propeller geometry in HSVA's 'geo*' and 'blp*' format(s).
    Openwater:
        Read the open-water data in HSVA's 'ow*' format.

Exceptions:
    BaseError:
        Raised if a column identifier from the input file is not known.
    BaseNotImplementedError:
        Raised if a column identifier is not implemented.


Created on Fri Jan 27 15:20:58 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

import collections
import argparse

import numpy as np
import scipy.integrate

from owscaling.plugins import (
        IPropellergeometryPlugin, IOWReaderPlugin, autoargs)
from owscaling.helpers import PolyFit, ApproxFit, Spline


pi = np.pi


class BaseError(ValueError):
    """Unknown column identifier."""
    def __init__(self, base, column, column_nr, filename):
        super().__init__(self,
             'Kennung "{}" für "{}" unbekannt; '
             'Spalte {} in "{}"'.format(
                     base, column, column_nr+1, filename))

class BaseNotImplementedError(NotImplementedError):
    """Column identifier not implemented."""
    def __init__(self, base, column, column_nr, filename):
        super().__init__(self,
             'Kennung "{}" für "{}" nicht implementiert; '
             'Spalte {} in "{}"'.format(
                     base, column, column_nr+1, filename))


class HSVAMixin(object):
    """
    This class provides basic functions to read HSVA data files.

    It should be inherited by classes which need to read data from files in
    HSVA's format, such as propeller geometry and openwater test data.

    Usage:
    =====

    From your class implementing the plug-in, inherit the plug-in interface
    and this HSVAMixin class (in that order) and call
        super(YOUR_CLASS, self).__init__(*args, **kwargs)
    early during your initialisation routine.

    To actually read the file, call your plug-in class as outlined in the
    plugins._IFilereader mix-in class.

    The _read() method is the workhorse and must be provided by your
    implementation. This is the place where the input is actually read as
    explained in the plugins._IFilereader documentation. There are some helper
    methods available to help you reading your input. These are listed below.

    Helper methods for reading the input:
    ------------------------------------

    findmagic(self, magic):
        Finds the line with the 'magic' token and stores the linenumber in
        the attribute 'magicline'.

    getline(self, line, absolute=False):
        Returns a line from the input.

        If 'absolute' is 'True' the line count starts at the beginning of the
        file, if 'False' (the default), the line count starts at the line of
        the magic key.

    getlines(self, start, end=None, absolute=False):
        Returns the lines from 'start' to 'end' (or the end of the file if
        'None') from 'self._filedata'

        For the meaning of the 'absolute' parameter, see 'getline()'.

    getvalue(self, line, typ=None, absolute=False):
        Returns the value from 'self._filedata' in 'line' and cast to type
        'typ'.

        For the meaning of the 'absolute' parameter, see 'getline()'.

    getheader(self, line, check=None, strict=False, absolute=False):
        Returns a dictionary with the headers and their position in the
        header line.

        If the 'check' parameter is supplied, it also checks if all tokens
        occur. It does not check if there are more columns available.

        If the 'strict' paramater is 'True', it also checks if there are not
        more columns available than given in the 'check' parameter.

        For the meaning of the 'absolute' parameter, see 'getline()'.

    gettable(self, start, end=None, typ=float, orientation='columns', absolute=False):
        Returns the values arranged in a table from line 'start' to 'end'
        (or the end of the file if 'None') as a list.

        Before returning the list, the values are cast to type 'typ'.

        The parameter 'orientation' works as following:
            'rows' - The returned list contains all the rows.
            'columns' - The returned list contains all columns.

        For the meaning of the 'absolute' parameter, see 'getline()'.

    Conventions:
    ===========

    Variable names:
    --------------

    Variables are named according to ITTC whereever possible. These variables
    names are in CAPITALS.

    An ending of "_data" denotes a list of data points.
    """
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    @property
    def magicline(self):
        """The index of the magic line."""
        return self._magicline

    def start(self, filedata, magic=None):
        """Store the line number with the 'magic' token."""
        self._filedata = filedata
        if magic:
            self._magicline = self._filedata.index(magic)
        else:
            self._magicline = 0

    def end(self):
        """End reading file and release allocated memory."""
        del self._filedata
        del self._magicline

    def getline(self, idx, absolute=False):
        """Retrieve a line from the file.

        Parameters:
            idx:
                Index of the line to be retrieved.
            absolute=False:
                If `True`, the line count starts at the beginning of the file;
                if `False`, the line count starts at the position of the magic
                line.

        Return:
            The line at index `idx`
        """

        return self._filedata[idx if absolute else idx + self.magicline]

    def getlines(self, start, end=None, absolute=False):
        """Retrieve some lines from the file.

        Parameters:
            start:
                Index of first line to be retrieved.
            end=None:
                Index of last line to be retrieved (or up to the end of the
                file if `None`).
            absolute=False:
                If `True`, the line count starts at the beginning of the file;
                if `False`, the line count starts at the position of the magic
                line.

        Return:
            The lines from `start` to `end`.
        """

        if end is not None and not absolute:
            last = end + self.magicline
        else:
            last = end
        return self._filedata[start if absolute else start+self.magicline:last]

    def getvalue(self, idx, typ=None, absolute=False):
        """Retrieve the value from some line from the file cast to type 'typ'.

        Parameters:
            idx:
                Index of the line where the value is stored.
            typ=None:
                The value retrieved is converted to type `typ`, if not `None`.
            absolute=False:
                If `True`, the line count starts at the beginning of the file;
                if `False`, the line count starts at the position of the magic
                line.

        Return:
            The value in line at index `idx` as type `typ`.
        """

        v = self.getline(idx, absolute=absolute).split()[0]
        if typ is not None:
            v = typ(v)
        return v

    def getheader(self, idx, check=None, strict=False, absolute=False):
        """Retrieve a dictionary with the headers and their position.

        Parameters:
            idx:
                Index of the line where the value is stored.
            check=None:
                If the `check` parameter is supplied (an iterable), it is
                checked, if all tokens actually occur. If there are more
                columns available than `check` has supplied, these additional
                columns are not checked.
            strict=False:
                If `True`, it is checked, if there are not more columns
                available than given in the `check` parameter.
            absolute=False:
                If `True`, the line count starts at the beginning of the file;
                if `False`, the line count starts at the position of the magic
                line.

        Return:
            A dictionary with the header token as key and its position as
            value.

        Exceptions:
            ValueError:
                * Raises a `ValueError`, if no header can be found in the file.
                * Raises a `ValueError`, if there are too many columns in the
                  file.
        """

        # Check header and try to be a bit clever
        headers = self.getline(idx, absolute=absolute).split()
        if check is not None:
            try:
                columns = {h: headers.index(h) for h in check}
            except ValueError as e:
                raise ValueError(
                    "Header '{}' cannot be found in data".format(
                        e.message.split("'")[1]))
            else:
                if strict and len(headers) != len(check):
                    raise ValueError(
                        'There are more columns available than checked for.')
            return columns
        else:
            return {h: i for i, h in enumerate(headers)}

    def gettable(self, start,
                 end=None, typ=float, orientation='columns', absolute=False):
        """Retrieve the values arranged in a table and cast to type `typ`.

        Parameters:
            start:
                Index of first line to be retrieved.
            end=None:
                Index of last line to be retrieved (or up to the end of the
                file if `None`).
            typ=float:
                The values retrieved are converted to type `typ`.
            orientation='columns':
                Specifies the orientation of the table as follows:
                    `'rows'`: The returned list is row-orientated:
                        [[row0], [row1], ...].
                    `'columns'`: The returned list column-orientated:
                        [[column0], [column1], ...].
            absolute=False:
                If `True`, the line count starts at the beginning of the file;
                if `False`, the line count starts at the position of the magic
                line.

        Return:
            A list with lists of values as type `typ`.

        Exceptions:
            ValueError:
                If the parameter `orientation` is neither `'columns'` nor
                `'rows'`.
        """

        # Read the lines, split them and convert them to type 'typ'.
        data = [[typ(t) for t in l.split()]
                for l in self.getlines(start, end=end)]

        if orientation == 'rows':
            return data
        elif orientation == 'columns':
            # See
            # https://stackoverflow.com/questions/6473679/python-list-of-lists-transpose-without-zipm-thing#comment-29535226
            # for this clever way to transpose a list.
            return [list(a) for a in zip(*data)]
        else:
            raise ValueError("Orientation '{}' not known".format(orientation))


class PropellerDetailed(IPropellergeometryPlugin, HSVAMixin):
    """Read the propeller geometry in HSVA's 'geo*' and 'blp*' format(s).

    Since the 'geo' format is missing essential information, such as the
    thickness distribution, a second file in 'blp' format can (more often than
    not: has) to be provided.

    For the format definitions of both file types see HSVA's documentation or
    example files supplied.
    """
    name = 'HSVA'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            details: {
                'nargs': '?',
                'type': argparse.FileType('r'),
                'help': 'file with more detailed propeller geometry',
                'metavar': 'FILE'
                } = None,
            *args, **kwargs):
        super().arguments(*args, **kwargs)


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Either save the `details` file or a dummy object with `name` and
        # `encoding` attributes (which are needed in the `__str__()` method)
        if self.args.details:
            self.has_details = True
            self.details = self.args.details
        else:
            self.has_details = False
            File = collections.namedtuple('File', ['name', 'encoding'])
            self.details = File(name=None, encoding=None)

    def __str__(self):
        return (super().__str__() +
        """+File:      {detailsname}
        +Encoding:  {detailsencoding}
        """.format(detailsname=self.get_filename(self.details),
                detailsencoding=self.get_fileencoding(self.details),
                self=self))

    def read(self):
        """Read the two HSVA files.

        The input can be a filename, a file object or any file-like object.

        Note:
        The final class must provide a '_read(self, f)' method. This method
        most probably wants to call 'findmagic()' and a combination of
        'getvalue()'s, 'getheader()' and 'gettable()'.

        """

        # Read the main geo file
        super().read()

        # Read the detailed blp file (see `plugins._IFilereader`)
        if self.has_details:
            if isinstance(self.details, str):   # Check if we got a string
                with open(self.details, 'r') as fo:
                    filedata = fo.readlines()
            else:
                filedata = self.details.readlines()

            filedata = [s.rstrip() for s in filedata]

            self._read_details(filedata)

    def _read(self, filedata):

        #
        # Read file data into arrays
        #

        # Start using the HSVAMixin handling the HSVA format. The propeller
        # geometry data starts with 'Relevent Design Geometry Data:'.
        self.start(filedata, magic='Relevant Design Geometry Data:')

        #: Hull string
        self.hullid = self.getvalue(2)
        #: Prop.-Num
        self.id = self.getvalue(3)
        #: Number of propeller blades [-]
        self.NPB = self.getvalue(4, int)
        #: Mean pitch to diameter ratio [-]
        self.PDR = self.getvalue(5, float)
        #: Blade area ratio [-]
        self.ADE = self.getvalue(6, float)
        #: Hub diameter ratio [-]
        self.XBDR = self.getvalue(7, float)
        #: Hand (0 for right, 1 for left) [0|1]
        self.hand = self.getvalue(8, int)

        headers = self.getheader(10, check=['r/R', 'c', 'P/D', 'rk/R'])
        table = self.gettable(11)

        #: Fractional radii data [-]
        X_data = table[headers['r/R']]
        #: Chord length distribution data c[r/R]/R [-]
        chr_data = table[headers['c']]
        #: Pitch distribution data P[r/R]/D [-]
        pdr_data = table[headers['P/D']]
        #: Rake distribution data rk[r/R]/R [-]
        rkr_data = table[headers['rk/R']]

        # End using the HSVAMixin handling the HSVA format
        self.end()


        #
        # Fit polynomials through c/R, PITCH/D and rake distribution
        #

        # We cannot use "strict=True" here, because more often than not the
        # hub diameter is smaller than the lowest section.

        # Fit c/R and P/D
        super().fit(X_data, chr_data, pdr_data)

        #: Rake distribution rk(r/R)/R [-]
        self.rkr = Spline(X_data, rkr_data, strict=False)


        #
        # Finalize by calculating some overall characteristics
        #

        # Calculate the actual blade area, because we do not want to rely on
        # the given blade area ratio.

        #: Calculated propeller area of one blade [-]
        self.ADE_blade = scipy.integrate.quad(self.chr, self.XBDR, 1)[0] / pi

    def _read_details(self, filedata):

        #
        # Read file data into arrays
        #

        # Start using the HSVAMixin handling the HSVA format. The detailed
        # propeller geometry data starts with the first line, so we do not
        # have to seach for some magic line.
        self.start(filedata, magic=None)

        #: Prop.-Num
        id_ = self.getvalue(0)
        if self.id != id_:
            raise ValueError(
                    'Inconsistent propeller ids: {} ({}) - {} ({})'.format(
                            self.id, self.get_filename(self.args.file),
                            id_, self.get_filename(self.details)))
        #: Propeller diameter [m]
        self.DP = self.getvalue(1, float)/1000
        #: Number of propeller blades [-]
        # NPB is always a float and not an int...
        NPB = self.getvalue(2).split('.')
        NPB_ = int(NPB[0])
        try:
            if NPB[1] and int(NPB[1]) != 0:
                raise ValueError(
                        'Number of propeller blades is not an integer.')
        except IndexError:
            pass
        if self.NPB != NPB_:
            raise ValueError(
                    'Inconsistent propeller blade numbers: '
                    '{} ({}) - {} ({})'.format(
                            self.NPB, self.get_filename(self.args.file),
                            NPB_, self.get_filename(self.details)))
        #: Mean pitch to diameter ratio [-]
        PDR_ = self.getvalue(3, float)
        if abs(self.PDR - PDR_) > 0.001:
            raise ValueError(
                    'Inconsistent pitch to diameter ratio: '
                    '{} ({}) - {} ({})'.format(
                            self.PDR, self.get_filename(self.args.file),
                            PDR_, self.get_filename(self.details)))
        #: Blade area ratio [-]
        ADE_ = self.getvalue(4, float)
        if abs(self.ADE - ADE_) > 0.001:
            raise ValueError(
                    'Inconsistent blade area ratio: '
                    '{} ({}) - {} ({})'.format(
                            self.ADE, self.get_filename(self.args.file),
                            ADE_, self.get_filename(self.details)))
        #: Hub diameter ratio [-]
        XBDR_ = self.getvalue(5, float)
        if abs(self.XBDR - XBDR_) > 0.001:
            raise ValueError(
                    'Inconsistent boss to diameter ratio: '
                    '{} ({}) - {} ({})'.format(
                            self.XBDR, self.get_filename(self.args.file),
                            XBDR_, self.get_filename(self.details)))

        # Number of lines to skip
        skip = self.getvalue(6, int)

        tablelines = self.getvalue(7+skip, int)
        table = self.gettable(8+skip, end=8+skip+tablelines)
        bases = self.gettable(8+skip+tablelines, end=8+skip+tablelines+1, typ=int)

        # The table has the following columns:
        # Radius, Chord length, CE, t_max, rake, pitch, camber
        #
        # The values in the `table` can have different meanings. The `bases`
        # row specifies these meanings. We want to save the values in
        # dimensionless form.

        RP_mm = self.DP*1000 / 2

        # Fractional radii data [-]
        i = 0
        base = bases[i][0]
        data = table[i]
        if base == 0:               # r/R:              <copy>
            X_data_ = data[:]
        elif base == 1:             # r [mm]:           /R[mm]
            X_data_ = [r/RP_mm
                       for r in data]
        elif base == 2:             # (r-Rh)/(R-Rh):    =x·(1-Rh/R) + Rh/R
            X_data_ = [r*(1-XBDR_) + XBDR_
                       for r in data]
        else:
            raise BaseError(base, 'r', i, self.get_filename(self.details))

        # Chord length distribution data c[r/R]/R [-]
        i = 1
        base = bases[i][0]
        data = table[i]
        if base == 0:               # c/D:              ·2
            chr_data_ = [2*c
                         for c in data]
        elif base == 1:             # c [mm]:           /R[mm]
            chr_data_ = [c/RP_mm
                         for c in data]
        elif base == 2:             # c [cm]:           ·10/R[mm]
            chr_data_ = [10*c/RP_mm
                         for c in data]
        elif base == 3:             # Teta (EK) [°]
            # TODO:
            raise BaseNotImplementedError(base, 'c', i,
                                          self.get_filename(self.details))
        else:
            raise BaseError(base, 'c', i, self.get_filename(self.details))

        # Distance of leading edge to generator line ce[r/R]/R [-]
        i = 2
        base = bases[i][0]
        data = table[i]
        if base == 0:               # Skew [°]:         ·rad(ψ)·x·R[mm]/R[mm]
            skew_data_ = [np.deg2rad(psi)*x - c/2
                          for psi, c, x in zip(data, chr_data_, X_data_)]
        elif base == 1:             # ce [mm]:          /R[mm]
            skew_data_ = [ce/RP_mm - c/2
                          for ce, c in zip(data, chr_data_)]
        elif base == 2:             # % Skew
            # TODO:
            raise BaseNotImplementedError(base, 'ce', i,
                                          self.get_filename(self.details))
        elif base == 3:             # Teta (AK) [°]
            # TODO:
            raise BaseNotImplementedError(base, 'ce', i,
                                          self.get_filename(self.details))
        elif base == 4:             # ce/D:             ·2
            skew_data_ = [2*ce - c/2
                          for ce, c in zip(data, chr_data_)]
        else:
            raise BaseError(base, 'ce', i, self.get_filename(self.details))

        # Thickness distribution data tmax[r/R]/chr[r/R] [-]
        i = 3
        base = bases[i][0]
        data = table[i]
        if base == 0:               # t/c:              <copy>
            xtc_data_ = data[:]
        elif base == 1:             # t [mm]:           /c[mm]=/(c·R[mm])
            xtc_data_ = [tm/(c*RP_mm)
            for tm, c in zip(data, chr_data_)]
        elif bases == 2:            # t/D:              ·D/c[mm]=·2R[mm]/(c·R[mm])=2/c
            xtc_data_ = [tm*2/c
                         for tm, c in zip(data, chr_data_)]
        elif bases == 3:            # t²c/10⁶·Z [mm³]   √(data·10⁶/(c·Z))
            xtc_data_ = [np.sqrt(tm*1e6/(c*NPB))
                         for tm, c in zip(data, chr_data_)]
        else:
            raise BaseError(base, 't', i, self.get_filename(self.details))

        # Rake distribution data rk[r/R]/R [-]
        i = 4
        base = bases[i][0]
        data = table[i]
        if base == 0:               # rk/D:             ·2
            rkr_data_ = [2*rk
                         for rk in data]
        elif base == 1:             # rk [mm]:          /R[mm]
            rkr_data_ = [rk/RP_mm
                         for rk in data]
        elif base == 2:             # Total Rake/D
            # TODO:
            raise BaseNotImplementedError(base, 'rk', i,
                                          self.get_filename(self.details))
        elif base == 3:             # Total Rake [mm]
            # TODO:
            raise BaseNotImplementedError(base, 'rk', i,
                                          self.get_filename(self.details))
        else:
            raise BaseError(base, 'rk', i, self.get_filename(self.details))

        # Pitch distribution data p[r/R]/D [-]
        i = 5
        base = bases[i][0]
        data = table[i]
        if base == 0:               # P/D:              <copy>
            pdr_data_ = data[:]
        elif base == 1:             # P [mm]:           /D[mm]=/(2R[mm])
            pdr_data_ = [p/(2*RP_mm)
                         for p in data]
        elif base == 2:             # pitch angle [°]:  =π·r/R·tanφ
            pdr_data_ = [pi*x*np.tan(np.deg2rad(phi))
                         for phi, x in zip(data, X_data_)]
        elif base == 3:             # P/Pm:             ·(Pₘ/D)
            pdr_data_ = [p*PDR_
                         for p in data]
        else:
            raise BaseError(base, 'p', i, self.get_filename(self.details))

        # Camber distribution data f[r/R]/chr [-]
        i = 6
        base = bases[i][0]
        data = table[i]
        if base == 0:               # f/c:              <copy>
            f_data_ = data[:]
        elif base == 1:             # f [mm]:           /c[mm]=/(c·R[mm])
            f_data_ = [f/(c*RP_mm)
                       for f, c in zip(data, chr_data_)]
        elif base == 2:             # f/D:              ·D/c[mm]=·2R[mm]/(c·R[mm])=2/c
            f_data_ = [f*2/c
                       for f, c in zip(data, chr_data_)]
        elif base == 3:             # f/t:              ·t[mm]/c[mm]=·t/c
            f_data_ = [f*xtc
                       for f, xtc in zip(data, xtc_data_)]
        else:
            raise BaseError(base, 'f', i, self.get_filename(self.details))

        # End using the HSVAMixin handling the HSVA format
        self.end()


        #
        # Fit polynomial through detailed c/R, t/R, f/c, PITCH/D and rake
        # distribution
        #

        # We cannot use "strict=True" here, because more often than not the
        # hub diameter is smaller than the lowest section.

        #: Chord length distribution c(r/R)/R []
        self.chr_ = ApproxFit(X_data_, chr_data_, strict=False)
        #: Distance of midchord point to generator line skew[r/R]/R [-]
        self.skew = ApproxFit(X_data_, skew_data_, strict=False)
        #: Thickness distribution data tmax[r/R]/R [-]
        self.xtc = ApproxFit(X_data_, xtc_data_, strict=False)
        #: Rake distribution rk(r/R)/R [-]
        self.rkr_ = Spline(X_data_, rkr_data_, strict=False)
        #: Pitch distribution p(r/R)/D [-]
        self.pdr_ = ApproxFit(X_data_, pdr_data_, strict=False)
        #: Camber distribution data f[r/R]/chr[r/R] [-]
        self.f = ApproxFit(X_data_, f_data_, strict=False)


        #
        # Finalize by calculating some overall characteristics
        #

        # Calculate the actual blade area, because we do not want to rely on
        # the given blade area ratio.

        #: Calculated propeller area of one blade [-]
        self.ADE_blade = scipy.integrate.quad(self.chr, self.XBDR, 1)[0] / pi


class Openwater(IOWReaderPlugin, HSVAMixin):
    """Read the open-water data in HSVA's 'ow*' format.

    For the format definition see HSVA's documentation or the example file
    supplied.

    Warning:
        * HSVA's OW file must not have an empty line at the end!
    """
    name = 'HSVA'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def _read(self, filedata):

        #
        # Read file data into arrays
        #

        # Start using the HSVAMixin handling the HSVA format. The propeller
        # geometry data starts with 'Relevant Test Data:'.
        self.start(filedata, magic='Relevant Test Data:')

        #: OW-Number
        self.id = self.getvalue(2)
        #: Diameter of model propeller [m]
        self.DP = self.getvalue(3, float)
        #: Pitch change for CPP
        self.deltaP = self.getvalue(4, float)
        #: Shaft revolutions [Hz]
        N = self.getvalue(5, float)
        self.N = lambda JEI: N
        #: Water temperature [°C]
        self.TEWA = self.getvalue(6, float)

        headers = self.getheader(8, check=['J', 'KT', '10KQ', 'KT_D'])
        table = self.gettable(9)

        #: Advance coefficients [-]
        self.JEI_data = table[headers['J']]
        #: Thrust coefficient data KT[J] [-]
        KT_data = table[headers['KT']]
        #: Torque coefficient data KQ[J] [-]
        KQ_data = [x/10 for x in table[headers['10KQ']]]
        #: Thrust data of nozzle KT_D[J] [-]
        KT_D_data = table[headers['KT_D']]

        # Store water temperature in water object.
        try:
            self.water.TEWA = self.TEWA
        except AttributeError:
            # Water not yet set
            pass

        # End using the HSVAMixin handling the HSVA format
        self.end()


        #
        # Fit polynomials through KT(J), KQ(J) and KT_D(J) distribution
        #

        # Fit KT(J) and KQ(J)
        super().fit(self.JEI_data, KT_data, KQ_data, N)

        #: Thrust coefficient of nozzle KT_D(J) [-]
        self.KT_D = PolyFit(self.JEI_data, KT_D_data)


if __name__ == '__main__':

    o = PropellerDetailed()
    o.read('../../docs/geo2188', '../../docs/blp2188')
    print(o)
