#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `StripExternal` implementing the `IScalingPlugin` interface.

Plug-ins:
    StripExternal:
        HSVA's external strip method.


Created on Tue Feb 21 12:08:35 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

import os
import subprocess
import tempfile

from owscaling.plugins import IScalingPlugin, autoargs


class StripExternal(IScalingPlugin):
    """HSVA's external strip method.

    Uses HSVA's external Windows program to scale open-water data.
    """
    name = 'ExtStreifenmethode'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            frictionlinechoice: {
                'type': int,
                'choices': [0, 1, 2],
                'help': 'Parameter for friction line choice ('
                        '0 for "Standard", '
                        '1 for "ITTC model line" and '
                        '2 for "as before with threshold"'
                        ')'
                },
            executable: {
                'args': ('--exe', '-x'),
                'help': 'external executable to be called (default: %s)s'
                } = 'stripqw.exe',
            *args, **kwargs):
        super().arguments(*args, **kwargs)


    def __str__(self):
        return (super().__str__() +
        """Executable: {self.args.executable}
        Friction:   {self.args.frictionlinechoice}
        """.format(self=self))

    def _scale(self):
        """Scale the open-water data."""

        #
        # Call external program
        #

        # call() and sisters require string arguments!
        ow_filename = self.openwater.get_filename()
        geo_filename = self.propeller.get_filename()
        subprocess.check_call(
                [self.args.executable,
                 str(self.SC),              # Scale
                 str(self.TEWA['sp']),      # Temperature self-propulsion
                 str(self.N['sp']),         # Shaft revs self-propulsion
                 str(self.N['fs']),         # Shaft revs full scale
                 ow_filename,               # open-water data
                 geo_filename,              # geometric data
                 str(self.args.frictionlinechoice)])

        #
        # Handle output files
        #

        # HSVA's external strip method always scales to self-propulsion and
        # full scale conditions (that is it does neither honour the
        # '--no-selfpropulsion' nor the '--no-fullscale' command line
        # argument). If we scale to self-propulsion and full scale condition,
        # everything is fine, ...
        if self.scale_to['sp'] and self.scale_to['fs']:
            # These two output files are hard coded
            files = {
                    'HSVA.csv': 'latest.csv',
                    'HSVA.ypp': 'latest.ypp'}

        # ... but if we don't want to scale to either self-propulsion
        # condition, we have to change the the output file, because it now
        # contains unwanted and wrong information.
        else:
            # Mangle the output files

            # Read the 'latest.ypp' file section-wise
            content = []
            with open('latest.ypp') as f:
                c = []
                for line in f:
                    c.append(line)
                    if line.strip() == '':
                        content.append(c)
                        c = []
                if c:
                    # Append last section if not empty
                    content.append(c)

            # Sections of .ypp file
            prolog = content[0]
            KT_ow = content[1]
            KQ_ow = content[2]
            KT_sp = content[3]
            KQ_sp = content[4]
            KT_fs = content[5]
            KQ_fs = content[6]
            epilog = content[7:]

            # Write mangled file into temporary file
            fd, filename = tempfile.mkstemp(dir='.', text=True)
            with open(filename, 'w') as t:

                t.writelines(prolog)
                t.writelines(KT_ow)
                t.writelines(KQ_ow)
                if self.scale_to['sp']:
                    t.writelines(KT_sp)
                    t.writelines(KQ_sp)
                else:
                    t.writelines(KT_ow)
                    t.writelines(KQ_ow)
                if self.scale_to['fs']:
                    t.writelines(KT_fs)
                    t.writelines(KQ_fs)
                else:
                    t.writelines(KT_ow)
                    t.writelines(KQ_ow)
                for e in epilog:
                    t.writelines(e)

            # Close the temporary file to keep Windows happy
            os.close(fd)

            # Move the temporary file back to 'latest.ypp'
            os.remove('latest.ypp')
            os.rename(filename, 'latest.ypp')

            # Remove 'latest.csv', because it was not dealt with
            os.remove('latest.csv')

            # Store it for the main program
            files = {
                    'HSVA.ypp': 'latest.ypp'}

        return files
