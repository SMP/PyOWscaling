#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `Viscosity` implementing the `IViscosityPlugin` interface.

Plug-ins:
    Viscosity:
        Kinematic viscosities of water according to HSVA.


Created on Thu Feb  2 14:49:13 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from owscaling.plugins import IViscosityPlugin


class ViscosityHSVATank(IViscosityPlugin):
    """Kinematic viscosity of tank water according to HSVA."""
    name = 'HSVA-Tank'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def __call__(self, TEWA, **kwargs):
        """Calculate the viscosity of tank water at temperature `TEWA`.

        Parameters:
            TEWA:
                The temperature of the water in [°C].

        Return:
            The kinematic viscosity [m²/s].
        """

        # Calculate the kinematic viscosity in m²/s according to HSVA.
        #    ν = (a·θ² + b·θ + c) / 10⁶
        #        a = +0.00057
        #        b = -0.04757
        #        c = +1.725
        return (0.00057 * TEWA**2 - 0.04757 * TEWA + 1.725) / 1e6


class ViscosityHSVASea(IViscosityPlugin):
    """Kinematic viscosity of sea water according to HSVA."""
    name = 'HSVA-Sea'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def __call__(self, TEWA, **kwargs):
        """Calculate the viscosity of sea water at temperature `TEWA`.

        Parameters:
            TEWA:
                The temperature of the water in [°C].

        Return:
            The kinematic viscosity [m²/s].
        """

        # Calculate the kinematic viscosity in m²/s according to HSVA.
        #    ν = (a·θ² + b·θ + c) / 10⁶
        #        a = +0.00057
        #        b = -0.04757
        #        c = +1.725
        return (0.00057 * TEWA**2 - 0.04757 * TEWA + 1.725) / 1e6
