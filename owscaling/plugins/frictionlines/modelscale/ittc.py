#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `ITTC` implementing the `IFrictionlinePlugin` interface for model
scale.

Plug-ins:
    ITTC:
        Frictional coefficient according to ITTC 1978 performance prediction
        method.


Created on Fri Jan 27 10:12:23 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from owscaling.plugins import IFrictionlinePlugin, autoargs


class ITTC(IFrictionlinePlugin):
    """Frictional coefficient (model scale) according to ITTC 1978 performance
    prediction method.

    Source:
        ITTC, Propulsion Committee of 27th ITTC 2014:
            ‘1978 ITTC Performance Prediction Method’.
            ITTC – Recommended Procedures and Guidelines, 1978, Effective Date
            2014, Revision 03.
    """
    name = 'ITTC-Model'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            factor: {
                'args': '--factor',
                'type': float,
                'choices': [0.04, 0.044],
                'help': 'factor of ITTC model scale friction line'
                } = 0.044,
            *args, **kwargs):
        super().arguments(*args, **kwargs)


    def __str__(self):
        return (super().__str__() +
        """Factor:     {self.args.factor}
        """.format(self=self))

    def __call__(self, RN, **kwargs):
        """Calculate the friction coefficient of a section.

        Parameters:
            RN:
                The Reynolds number based on the section chord length [-].

        Return:
            The frictional coefficient [-].
        """

        return self.args.factor/RN**(1/6) - 5/RN**(2/3)
