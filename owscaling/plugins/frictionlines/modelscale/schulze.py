#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `Schulze` implementing the `IFrictionlinePlugin` interface for model
scale.

Plug-ins:
    Schulze:
        Frictional coefficient (model scale) according to Schulze.

Created on Fri Feb  3 14:13:40 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from numpy import log as ln

from owscaling.plugins import IFrictionlinePlugin


class Schulze(IFrictionlinePlugin):
    """Frictional coefficient (model scale) according to Schulze.

    Note:
        There is a mistake in the presentation referenced below. The centre
        part of the friction line cannot be 0.03, but must be 0.003, because
        0.3/∛Rn = 0.003 and 3.913/ln(Re)²'⁵⁸ - 1700/Re = 0.003056!

    Source:
        Schulze, R.:
            ‘Neue Verfahren zur Reynoldszahlkorrektur für Freifahrtmessungen
            an Modellpropellern’.
            Presentation STG-Sprechtag „Moderne Propulsionskonzepte“, 2016,
            Hamburg, Germany.
    """
    name = 'Schulze'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def __call__(self, RN, **kwargs):
        """Calculate the friction coefficient of a section.

        Parameters:
            RN:
                The Reynolds number based on the section chord length [-].

        Return:
            The frictional coefficient [-].
        """

        if RN <= 1e6:
            return 0.3 * RN**(-1/3)
        elif RN <= 1.7e6:
            return 0.003
        else:
            return 3.913/ln(RN)**(2.58) - 1700/RN
