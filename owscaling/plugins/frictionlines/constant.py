#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `Constant` implementing the `IFrictionlinePlugin` interface.

Plug-ins:
    Constant:
        Constant frictional coefficient.


Created on Thu Feb  2 16:11:52 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from owscaling.plugins import IFrictionlinePlugin, autoargs
from owscaling.helpers import LimitedFloat


class Constant(IFrictionlinePlugin):
    """Constant frictional coefficient."""
    name = 'Constant'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            const: {
                'type': LimitedFloat(min=0),
                'help': 'constant value of friction line'},
            *args, **kwargs):
        super().arguments(*args, **kwargs)


    def __str__(self):
        return (super().__str__() +
        """Const:      {self.args.const}
        """.format(self=self))

    def __call__(self, RN, **kwargs):
        """Calculate the friction coefficient of a section.

        Parameters:
            RN:
                The Reynolds number based on the section chord length [-].

        Return:
            The frictional coefficient [-].
        """

        return self.args.const
