#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `Prandtl` implementing the `IFrictionlinePlugin` interface for full
scale.

Plug-ins:
    Prandtl:
        Frictional coefficient according to Prandtl.


Created on Fri Jan 27 10:12:23 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from numpy import log10

from owscaling.plugins import IFrictionlinePlugin


class Prandtl(IFrictionlinePlugin):
    """Frictional coefficient (full scale) according to Prandtl.

    Source:
        Schlichting, H. & Gersten, K. (2006).
            'Grenzschicht-Theorie'. Springer Verlag, Berlin, Germany.
    """
    name = 'Prandtl'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def __call__(self, RN, **kwargs):
        """Calculate the friction coefficient of a section.

        Parameters:
            RN:
                The Reynolds number based on the section chord length [-].

        Return:
            The frictional coefficient [-].
        """

        return 0.455 / log10(RN)**2.58
