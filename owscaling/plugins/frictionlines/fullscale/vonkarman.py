#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in `VonKarman` implementing the `IFrictionlinePlugin` interface for full
scale.

Plug-ins:
    VonKarman:
        Frictional coefficient according to von Kármán's local skin friction
        coefficient.

Bug:
    There is no solution if RNₓ→0, that is near the leading edge! Any
    suggestions how to solve it anyway?


Created on Tue Feb  7 14:06:52 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from numpy import log10, sqrt
import scipy.optimize
import scipy.integrate

from owscaling.plugins import IFrictionlinePlugin


class VonKarman(IFrictionlinePlugin):
    """Frictional coefficient (full scale) according to von Kármán's local
    skin friction coefficient.

    Bug:
        There is no solution if RNₓ→0, that is near the leading edge! Any
        suggestions how to solve it anyway?

    Source:
        von Kármán, Th.:
            ‘Turbulence and Skin Friction’.
            J. of the Aeronautical Sciences, 1934, Vol. 1, No. 1, pp. 1-20.
    """
    name = 'von-Kármán'
    __version__ = '0.0.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'


    def cf(self, RN_x):
        """Local skin friction coefficient.

        Parameters:
            RN_x:
                The local Reynolds number with respect to the running
                length from the leading edge [-].

        Return:
            The local frictional coefficient [-].

        Exceptions:
            scipy.optimize.nonlin.NoConvergence:
                The solver for the von Kármán line does not converge.
        """

        # Conditional equation for cf, which must be 0
        func = lambda cf, RN_x: 4.15 * log10(RN_x*cf) + 1.7 - 1/sqrt(cf)

        # 1st derivative of `func`
        fprime = lambda cf, RN_x: 1/(2 * cf**(3/2)) + 1.80232/cf

        # 2nd derivative of `func`
        fprime2 = lambda cf, RN_x: -0.75/cf**(5/2) - 1.80232/cf**2

        # Starting point (using ITTC's frictional coefficient)
        cf0 = 0.075 * (1 - 0.869/(log10(RN_x) - 2)) * (log10(RN_x) - 2)**(-2)

        try:
            cf = scipy.optimize.newton(
                    func, cf0,
                    fprime=fprime, fprime2=fprime2,
                    args=(RN_x,))
        except RuntimeError:
            return 0

        if abs(func(cf, RN_x)) <= 1.48e-08:
            return cf
        else:
            raise scipy.optimize.NoConvergence(
                    'The local friction coefficient found is not a root of '
                    'the conditional equation: f({}) = {}'.format(
                            cf, func(cf, RN_x)))

    def cf_x(self, x, RN):
        """Local skin friction coefficient.

        Parameters:
            x:
                Non-dimensional running length from the leading edge [0..1].
            RN:
                The Reynolds number based on the section length [-].

        Return:
            The local frictional coefficient [-].
        """

        return self.cf(x*RN)

    def __call__(self, RN, **kwargs):
        """Calculate the friction coefficient of a section.

        Parameters:
            RN:
                The Reynolds number based on the section length [-].

        Return:
            The frictional coefficient [-].
        """

        # Integrate the local skin friction coefficient cf over the section
        # length
        return scipy.integrate.quad(self.cf_x, 0, 1, args=(RN,))[0]


if __name__ == '__main__':

    fl = VonKarman({})
    print(fl)

    RN = 1e9            # Reynolds number [-]

    print('Local skin friction coefficient cf for')
    print('    RN={:g}'.format(RN))
    print('ITTC 1957: ', 0.075 * (1 - 0.869/(log10(RN) - 2)) * (log10(RN) - 2)**(-2))
    print('von Kármán:', fl.cf(RN))
    print()

    print('Friction coefficient cF for')
    print('    RN={:g}'.format(RN))
    print('ITTC 1957: ', 0.075 / (log10(RN)-2)**2)
    print('von Kármán:', fl(RN))
