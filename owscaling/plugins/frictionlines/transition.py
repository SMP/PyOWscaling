#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-ins `TransitionFrictionXXX` implementing the `IFrictionlinePlugin`
interface.

Plug-ins:
    TransitionFrictionOW:
        Local friction coefficient with transition point integrated over the
        chord length for the open-water condition with undisturbed inflow.
    TransitionFrictionBehind:
        Local friction coefficient with transition point integrated over the
        chord length for the behind condition with disturbed inflow.


Created on Fri Feb 10 20:48:53 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

from numpy import log as ln    # np.log: Natural logarithm
import scipy.optimize

from owscaling.plugins import IFrictionlinePlugin, autoargs
from owscaling.helpers import LimitedFloat


class TransitionMixin(object):
    """Local friction coefficient with transition point integrated over the
    chord length.

    Local friction coefficients cf
    ==============================

    The local friction coefficient cf depend on the local Reynolds number
        Rnₓ = vx/ν
    with x the distance from the leading edge.

    The local friction coefficient is approximated with the formula
        cf = a/(Rnₓ)^b
    with the factors a and b different for laminar and turbulent flow.

    Up to the transition point xₜ the flow is considered laminar and
    afterwards turbulent. This transition happens instantaneously. The
    transistion point is defined by its local Reynolds number Rnₜ (this is
    supplied as parameter at the command line).

    The local Reynolds number for the turbulent flow has to be adapted so
    that the impulse loss thickness δ₂ is the same for the laminar and the
    turbulent boundary layer. This results in the following conditional
    equation for Rnₐ [Schlichting 2006, eq. (18.101)]:
        0.664√Rnₜ = (Rnₜ - Rnₐ)[ϰ/ln(Rnₜ - Rnₐ)·G(Λ, D)]²
    with
        Λ = ln(Rnₜ - Rnₐ)
        D = 2·lnϰ + ϰ·(C⁺-3)
             ⎧ ~5       kₛ⁺ ≤ 5         hydraulically smooth
        C⁺ = ⎨ C⁺(kₛ⁺)  5 < kₛ⁺ < 70    transition
             ⎩ ~8       70 ≤ kₛ⁺        rough
    The conditional equation for G(Λ, D) is [Schlichting 2006, eq. (17.60)]:
        Λ/G + 2·ln(Λ/G) - D = Λ

    Total friction coefficient cF
    =============================

    The local friction coefficient can be integrated analytically to safe
    computation time to give the frictional coefficient of the section.

    The Reynolds number of the whole section
        Rn = vc/ν
    and the local Reynolds number
        Rnₓ = vx/ν
    with x as the running length [0..c], which becomes with
        ξ = x/c ↔ x = ξc,
    the dimensionless running length (0..1),
        Rnₓ = vξc/ν = ξRn
    The transistion point xₜ can be calculated as
        Rnₜ = vxₜ/ν = vξₜc/ν = ξₜRn
        ξₜ = Rnₜ/Rn

    The friction coefficent cF is the integrated local friction coefficient cf:
        cF = 1/c·∫cf dx                 [∫ 0..c]
        cF = 1/c·∫cf dξc = ∫cf dξ       [∫ 0..1]
        cF = ∫a/Rnₓᵇ dξ
        cF = ∫a/(ξRn)ᵇ dξ
        cF = a/Rnᵇ ∫ξ⁻ᵇ dξ
        cF = a/Rnᵇ ξ¹⁻ᵇ/(1-b) + const
    For the laminar part the boundaries are [0..ξₜ]:
        cFlam = a/Rnᵇ ξₜ¹⁻ᵇ/(1-b)
    For the turbulent part Rnₓ has to be replaced with
        Rnₓ-Rnₐ = ξRn-Rnₐ
    and hence
        cF = a/[(1-b)·Rn]·(ξRn-Rnₐ)¹⁻ᵇ + const
    The boundaries are [ξₜ..1]:
        cFturb = a/[(1-b)·Rn]·[(Rn-Rnₐ)¹⁻ᵇ - (ξₜRn-Rnₐ)¹⁻ᵇ]
        cFturb = a/[(1-b)·Rn]·[(Rn-Rnₐ)¹⁻ᵇ - (Rnₓ-Rnₐ)¹⁻ᵇ]

    Sources:
        Schlichting, H. & Gersten, K.:
            Grenzschicht-Theorie.
            Berlin, Germany, 2006: Springer Verlag.
        Streckwall, H., Greitsch, L., Müller, J., Scharf, M. & Bugalski, T.:
            ‘Development of a Strip Method Proposed as New Standard for
            Propeller Performance Scaling’.
            Ship Technology Research, 2013, Vol. 60, pp. 58–69.
    """
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(self,
            Aturb: {
                'args': '--Aturb',
                'type': float,
                'help': 'factor a for the local turbulent friction '
                        'coefficient CF_x = a/(Rn_x)^b'
                } = 0.0592,
            Bturb: {
                'args': '--Bturb',
                'type': float,
                'help': 'factor b for the local turbulent friction '
                        'coefficient CF_x = a/(Rn_x)^b'
                } = 0.2,
            kappa: {
                'args': '--karman',
                'type': float,
                'help': 'von Kármán constant ϰ'
                } = 0.41,
            Cplus: {
                'args': '--cplus',
                'type': float,
                'help': 'integration constant C⁺'
                } = 5
            ):
        pass


    def __str__(self):
        return """a (turb):       {self.args.A_turb}
        b (turb):       {self.args.B_turb}
        RN (trans, SS): {self.args.RN_t_ss}
        RN (trans, PS): {self.args.RN_t_ps}
        """.format(self=self)

    def __call__(self, RN, **kwargs):
        """Calculate the friction coefficient of a section.

        Parameters:
            RN:
                The Reynolds number based on the section chord length [-].

        Return:
            The frictional coefficient [-].
        """

        # Transition points for suction and pressure side
        x_t_ss = self.args.RN_t_ss / RN
        x_t_ps = self.args.RN_t_ps / RN

        # Return mean value, because all other friction lines return the
        # friction of ONE side
        return (self.cF_singleside(x_t_ss, RN) +
                self.cF_singleside(x_t_ps, RN)) / 2

    def cF_singleside(self, x_t, RN):
        """Calculate the total friction coefficient of one side.

        Parameters:
            x_t:
                Relative distance from leading edge, where the transition from
                laminar to turbulence occurs [0..1].
            RN:
                The Reynolds number based on the plate length [-].

        Return:
            The frictional coefficient [-].
        """

        if x_t >= 1:

            # Fully laminar over the whole section
            a = 0.664
            b = 0.5
            cF_lam = a / RN**b / (1-b)
            return cF_lam

        else:

            RN_t = x_t * RN
            RN_a = self.RN_a(RN_t)

            # Total laminar friction coefficient of section:
            a = 0.664
            b = 0.5
            cF_lam = a / RN**b * x_t**(1-b) / (1-b)

            # Total turbulent friction coefficient of section:
            a = self.A_turb
            b1 = 1-self.B_turb
            cF_turb = a / (b1*RN) * ((RN-RN_a)**b1 - (RN_t-RN_a)**b1)

            # Laminar and turbulent flow
            return cF_lam + cF_turb

    def RN_a(self, RN_t):
        """Calculate the Reynolds number Rnₐ.

        The local Reynolds number for the turbulent flow has to be adapted by
        Rnₐ so that the impulse loss thickness δ₂ in the transition point is
        the same for a purely laminar and purley turbulent boundary layer.

        See this class's documentation.

        Parameters:
            RN_t:
                The local Reynolds number in the transition point [-].

        Return:
            The Reynolds number Rnₐ [-].
        """

        kappa = self.args.kappa
        Cplus = self.args.Cplus

        D = 2 * ln(kappa) + kappa * (Cplus - 3)

        RN_a = scipy.optimize.newton(
                self.cond_equ_RN_a, 2*RN_t/3,
                # No primes here, because of cond_equ(Rnₐ, G(Λ(Rnₐ)))
                args=(RN_t, D))

        if abs(self.cond_equ_RN_a(RN_a, RN_t, D)) <= 1.48e-08:
            return RN_a
        else:
            # If Newton does not converge, maybe Brent's method does. In this
            # case we have to supply a bracketing interval. The obvious choice
            # would be [0, Rnₜ], but unfortunately this would result in ln(0),
            # which is +∞; we go close to it.
            RN_a = scipy.optimize.brentq(
                    self.cond_equ_RN_a, 0, 0.99*RN_t,
                    args=(RN_t, D))

    def cond_equ_RN_a(self, RN_a, RN_t, D):
        """Calculate the conditional equation for Rnₐ."""

        delta_RN = RN_t - RN_a
        Lambda = ln(delta_RN)
        cf_t = 0.664 * RN_t**0.5

        return delta_RN * (self.kappa / Lambda * self.G(Lambda, D))**2 - cf_t

    def G(self, Lambda, D):
        """Calculate the conditional equation for G."""

        # Conditional equation for G(Λ, D)
        func = lambda G, Lambda, D: (
                Lambda/G +
                2 * ln(Lambda/G) -
                D -
                Lambda)

        fprime = lambda G, Lambda, D: -(Lambda + 2*G)/G**2

        fprime2 = lambda G, Lambda, D: 2 * (Lambda + G)/G**3

        G = scipy.optimize.newton(
                func, 2,
                fprime=fprime, fprime2=fprime2,
                args=(Lambda, D))

        if abs(func(G, Lambda, D)) <= 1.48e-08:
            return G
        else:
            raise scipy.optimize.nonlin.NoConvergence(
                    'The friction coefficient found is not a root of the '
                    'conditional equation: f({}) = {}'.format(
                            G, func(G, Lambda, D)))


class TransitionFrictionOW(IFrictionlinePlugin, TransitionMixin):
    """Local friction coefficient with transition point integrated over the
    chord length for the open-water condition with undisturbed inflow.
    """
    name = 'Transition-OW'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            RN_t_SS: {
                'args': '--RN_t_SS',
                'type': LimitedFloat(min=0, endpoints=']]'),
                'help': 'local Reynolds number of transition point '
                        'on the suction side'
                } = 4e5,
            RN_t_PS: {
                'args': '--RN_t_PS',
                'type': LimitedFloat(min=0, endpoints=']]'),
                'help': 'local Reynolds number of transition point '
                        'on the pressure side'
                } = 2e5,
            *args, **kwargs):
        IFrictionlinePlugin.__init__(self, *args, **kwargs)


    def __init__(self, *args, **kwargs):
        IFrictionlinePlugin.__init__(self, *args, **kwargs)
        TransitionMixin.__init__(self)

    def __str__(self):
        return IFrictionlinePlugin.__str__(self)+TransitionMixin.__str__(self)


class TransitionFrictionBehind(IFrictionlinePlugin, TransitionMixin):
    """Local friction coefficient with transition point integrated over the
    chord length for the behind condition with disturbed inflow.
    """
    name = 'Transition-Behind'
    __version__ = '3.1.0'
    __copyright__ = 'Copyright (C) 2017  Stephan Helma'

    @autoargs
    def arguments(
            self,
            RN_t_SS: {
                'args': '--RN_t_SS',
                'type': LimitedFloat(min=0, endpoints=']]'),
                'help': 'local Reynolds number of transition point '
                        'on the suction side'
                } = 3e5,
            RN_t_PS: {
                'args': '--RN_t_PS',
                'type': LimitedFloat(min=0, endpoints=']]'),
                'help': 'local Reynolds number of transition point '
                        'on the pressure side'
                } = 1e5,
            *args, **kwargs):
        IFrictionlinePlugin.arguments(self, *args, **kwargs)


    def __init__(self, *args, **kwargs):
        IFrictionlinePlugin.__init__(self, *args, **kwargs)
        TransitionMixin.__init__(self)

    def __str__(self):
        return IFrictionlinePlugin.__str__(self)+TransitionMixin.__str__(self)


if __name__ == '__main__':

    from numpy import log10

    fl = TransitionFrictionOW({})
    print(fl)

    RN = 1e9            # Reynolds number [-]
#    XKPLCH = 0.00001    # Ratio roughness to section length [-]

#    LCH = fl.KP/XKPLCH  # Section length [m]

    f1 = '{:<6} {:<14} {:<14} {:<10} {:<10}'
    f = '{:<6.0e} {:<14.4} {:<14.4} {:<10.4} {:<10.4}'
    print('Friction coefficient for')
#    print('    LCH={}m'.format(LCH))
#    print('    k={:g}m'.format(fl.KP))
#    print('    k/LCH={:g}'.format(XKPLCH))
    print(f1.format('RN', 'ITTC1978 ship', 'ITTC1978 m', 'ITTC1957', 'Transition'))
    for i in range(4,11):
        RN = 10**i
        print(f.format(
                RN,
                0., #(1.89 + 1.62*log10(LCH / fl.KP))**(-2.5),
                0.044/RN**(1/6) - 5/RN**(2/3),
                0.075 / (log10(RN)-2)**2,
                fl(RN)))
