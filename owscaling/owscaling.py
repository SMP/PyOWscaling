#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module provides the helper class `OWScaling` to simplify the usage of
the PyOWScaling framework.

Usage:
    >>> import owscaling
    >>> owscaling = owscaling.OWScaling(...)
    >>> owscaling.scale()
    >>> owscaling.write()

Classes:
    OWScaling:
        Thin wrapper around a `IScalingPlugin` plug-in.


Created on Tue Jun  6 09:50:35 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__version__ = '3.1.0'

import os


class OWScaling(object):
    """Thin wrapper around a `IScalingPlugin` plug-in.

    This thin wrapper helps writing programs using the scaling plug-in. After
    setting all but the `output` (and `output_files`) properties either during
    the class initialization or later by setting these properties explicitely,
    a call to `scale()` takes proper care of

    Properties:
        scaling:
            Scaling method plug-in (type `IScalingPlugin`).
        propeller:
            Propeller geometry plug-in (type `IPropellergeometryPlugin`).
        openwater:
            Open-water reader plug-in (type `IOWReaderPlugin`).
        viscosity_ow:
            Water viscosity plug-in for open-water (type `IViscosityPlugin`).
        viscosity_sp:
            Water viscosity plug-in for self-propulsion (type
            `IViscosityPlugin`).
        viscosity_fs:
            Water viscosity plug-in for full scale (type `IViscosityPlugin`).
        density_ow:
            Water density plug-in for open-water (type `IDensityPlugin`).
        density_sp:
            Water density plug-in for self-propulsion (type `IDensityPlugin`).
        density_fs:
            Water density plug-in for full scale (type `IDensityPlugin`).
        friction_ow:
            Friction line plug-in for open-water (type `IFrictionlinePlugin`).
        friction_sp:
            Friction line plug-in for self-propulsion (type
            `IFrictionlinePlugin`).
        friction_fs:
            Friction line plug-in for full scale (type `IFrictionlinePlugin`).
        formfactor:
            Section formfactor plug-in (type `IFormfactorPlugin`).
        outputs:
            Output writer plug-ins (type `IOWWriterPlugin`).
        output_files:
            Output files written by scaling method.

    Methods:
        read():
            Read the input files.
        initialize():
            Initialize the scaling method.
        scale(SC, Ns, TEWAs):
            Scale the open-water test.
        write(clean=True):
            Save scaled data to files.
    """

    @property
    def scaling(self):
        """Scaling method plug-in (type `IScalingPlugin`)."""
        return self._scaling

    @scaling.setter
    def scaling(self, value):
        # Ad instance (and not class)
        if isinstance(value, type):
            self._scaling = value()
        else:
            self._scaling = value
        self.__initialized = False

    @scaling.deleter
    def scaling(self):
        delattr(self, '_scaling')
        self.__initialized = False

    @property
    def propeller(self):
        """Propeller geometry plug-in (type `IPropellergeometryPlugin`)."""
        return self._propeller

    @propeller.setter
    def propeller(self, value):
        self._propeller = value
        self.__read = False
        self.__initialized = False

    @propeller.deleter
    def propeller(self):
        delattr(self, '_propeller')
        self.__read = False
        self.__initialized = False

    @property
    def openwater(self):
        """Open-water reader plug-in (type `IOWReaderPlugin`)."""
        return self._openwater

    @openwater.setter
    def openwater(self, value):
        self._openwater = value
        self.__read = False
        self.__initialized = False

    @openwater.deleter
    def openwater(self):
        delattr(self, '_openwater')
        self.__read = False
        self.__initialized = False

    @property
    def viscosity_ow(self):
        """Water viscosity plug-in for open-water (type `IViscosityPlugin`)."""
        return self._viscosity_ow

    @viscosity_ow.setter
    def viscosity_ow(self, value):
        self._viscosity_ow = value
        self.__initialized = False

    @viscosity_ow.deleter
    def viscosity_ow(self):
        delattr(self, '_viscosity_ow')
        self.__initialized = False

    @property
    def viscosity_sp(self):
        """Water viscosity plug-in for self-propulsion (type `IViscosityPlugin`)."""
        return self._viscosity_sp

    @viscosity_sp.setter
    def viscosity_sp(self, value):
        self._viscosity_sp = value
        self.__initialized = False

    @viscosity_sp.deleter
    def viscosity_sp(self):
        delattr(self, '_viscosity_sp')
        self.__initialized = False

    @property
    def viscosity_fs(self):
        """Water viscosity plug-in for full scale (type `IViscosityPlugin`)."""
        return self._viscosity_fs

    @viscosity_fs.setter
    def viscosity_fs(self, value):
        self._viscosity_fs = value
        self.__initialized = False

    @viscosity_fs.deleter
    def viscosity_fs(self):
        delattr(self, '_viscosity_fs')
        self.__initialized = False

    @property
    def density_ow(self):
        """Water density plug-in for (type `IDensityPlugin`)."""
        return self._density_ow

    @density_ow.setter
    def density_ow(self, value):
        self._density_ow = value
        self.__initialized = False

    @density_ow.deleter
    def density_ow(self):
        delattr(self, '_density_ow')
        self.__initialized = False

    @property
    def density_sp(self):
        """Water density plug-in for (type `IDensityPlugin`)."""
        return self._density_sp

    @density_sp.setter
    def density_sp(self, value):
        self._density_sp = value
        self.__initialized = False

    @density_sp.deleter
    def density_sp(self):
        delattr(self, '_density_sp')
        self.__initialized = False

    @property
    def density_fs(self):
        """Water density plug-in for (type `IDensityPlugin`)."""
        return self._density_fs

    @density_fs.setter
    def density_fs(self, value):
        self._density_fs = value
        self.__initialized = False

    @density_fs.deleter
    def density_fs(self):
        delattr(self, '_density_fs')
        self.__initialized = False

    @property
    def friction_ow(self):
        """Friction line plug-in for open-water (type `IFrictionlinePlugin`)."""
        return self._friction_ow

    @friction_ow.setter
    def friction_ow(self, value):
        self._friction_ow = value
        self.__initialized = False

    @friction_ow.deleter
    def friction_ow(self):
        delattr(self, '_friction_ow')
        self.__initialized = False

    @property
    def friction_sp(self):
        """Friction line plug-in for self-propulsion (type `IFrictionlinePlugin`)."""
        return self._friction_sp

    @friction_sp.setter
    def friction_sp(self, value):
        self._friction_sp = value
        self.__initialized = False

    @friction_sp.deleter
    def friction_sp(self):
        delattr(self, '_friction_sp')
        self.__initialized = False

    @property
    def friction_fs(self):
        """Friction line plug-in for full scale (type `IFrictionlinePlugin`)."""
        return self._friction_fs

    @friction_fs.setter
    def friction_fs(self, value):
        self._friction_fs = value
        self.__initialized = False

    @friction_fs.deleter
    def friction_fs(self):
        delattr(self, '_friction_fs')
        self.__initialized = False

    @property
    def formfactor(self):
        """Section formfactor plug-in (type `IFormfactorPlugin`)."""
        return self._formfactor

    @formfactor.setter
    def formfactor(self, value):
        self._formfactor = value
        self.__initialized = False

    @formfactor.deleter
    def formfactor(self):
        delattr(self, '_formfactor')
        self.__initialized = False

    @property
    def outputs(self):
        """Output writer plug-ins (type `IOWWriterPlugin`)."""
        return self._outputs

    @outputs.setter
    def outputs(self, value):
        self._outputs = value
        self.__initialized = False

    @outputs.deleter
    def outputs(self):
        delattr(self, '_outputs')
        self.__initialized = False

    @property
    def output_files(self):
        """Output files written by scaling method."""
        return self._output_files


    def __init__(self, scaling,
                 propeller=None, openwater=None,
                 viscosities=None, densities=None,
                 frictions=None,
                 formfactor=None,
                 outputs=[]):
        """
        Parameters:
            scaling:
                Scaling method plug-in (type `IScalingPlugin`).
            propeller=None:
                Propeller geometry plug-in (type `IPropellergeometryPlugin`).
            openwater=None:
                Open-water reader plug-in (type `IOWReaderPlugin`).
            viscosities=None:
                Iterable with water viscosity plug-ins (type
                `IViscosityPlugin`) for open-water, self-propulsion and full
                scale conditions.
            densities=None:
                Iterable with water density plug-ins (type `IDensityPlugin`)
                for open-water, self-propulsion and full scale conditions.
            frictions=None:
                Iterable with friction line plug-ins (type
                `IFrictionlinePlugin`) for open-water, self-propulsion and
                full scale conditions.
            formfactor=None:
                Section formfactor plug-in (type `IFormfactorPlugin`).
            outputs=None:
                Iterable with output writer plug-ins (type `IOWWriterPlugin`).
        """

        self.__read = False
        self.__initialized = False

        if scaling:
            self.scaling = scaling
        if propeller:
            self.propeller = propeller
        if openwater:
            self.openwater = openwater
        if viscosities:
            self.viscosity_ow, self.viscosity_sp, self.viscosity_fs = viscosities
        if densities:
            self.density_ow, self.density_sp, self.density_fs = densities
        if frictions:
            self.friction_ow, self.friction_sp, self.friction_fs = frictions
        if formfactor:
            self.formfactor = formfactor
        if outputs:
            self.outputs = outputs

    def read(self):
        """Read the input files."""
        self.openwater.read()
        self.propeller.read()

    def initialize(self):
        """Initialize the scaling method."""
        self.scaling.initialize(
                self.propeller,
                self.openwater,
                (self.viscosity_ow, self.viscosity_sp, self.viscosity_fs),
                # XXX:
                # Remove/Use density plug-ins?
                # (self.density_ow, self.density_sp, self.density_fs),
                (None, None, None),
                (self.friction_ow, self.friction_sp, self.friction_fs),
                self.formfactor)

    def scale(self, SC, Ns, TEWAs):
        """Scale the open-water test.

        It saves the output files (if any) in `self.output_files`.

        Parameters:
            SC:
                The model scale λ.
            N:
                A tuple with the propeller revolutions of the self-propulsion
                test and in full scale [Hz], e.g. (N_sp, N_fs).
            TEWA:
                A tuple with the water temperature of the self-propulsion test
                and in full scale [°C], e.g. (TEWA_sp, TEWA_fs).
        """

        if not self.__read:
            self.read()
        if not self.__initialized:
            self.initialize()
        self._output_files = self.scaling.scale(
                SC, Ns, TEWAs)

    def write(self, clean=True):
        """Save scaled data to files.

        Parameters:
            clean=True:
                If `True`, clean up by removing all files written by the
                scaling method which were not renamed.

        Exceptions:
            ValueError:
                If the scaling method did not save the output in a file with
                the expected name.
        """

        if self.output_files:
            # Scaling method wrote its own file(s)

            # Rename output files written by the scaling method according to the
            # command line
            for output in self.outputs:
                # Get the name of the output file corresponding to the output
                # plug-in
                try:
                    filename = self.output_files.pop(output.name)
                except KeyError:
                    raise ValueError(
                            'Scaling method did not save a "{}" file'.format(
                                    output.name))
                # Rename the file
                output.move(filename)

            if clean:
                # Remove all remaining files
                for filename in self.output_files:
                    os.remove(filename)

        else:
            for output in self.outputs:
                output.write(self.scaling)

