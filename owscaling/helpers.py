#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A collection of helper for PyOWscaling.

This collection include:
    * Helpers for argument parser `argparse`:
        * Class `ArgumentDefaultsHFormatter`
        * Class `DocumentationAction`
        * Class `OWArgumentParser`
        * Class `LimitedMixin`
        * Class `LimitedInt(LimitedMixin)`
        * Class `LimitedFloat(LimitedMixin)`
    * Helper for writing output files
        * Class `FortranPrinter`
    * Helper for fitting curves to data points
        * Class `RangeCheckMixin`
        * Class `PolyFit(..., RangeCheckMixin)
        * Class `ApproxFit(..., RangeCheckMixin)


Created on Wed Feb  1 09:55:47 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__version__ = '3.1.0'


import collections
import argparse

import numpy as np
import scipy.interpolate


# = Helpers for argument parser `argparse` ====================================

class ArgumentDefaultsHFormatter(argparse.ArgumentDefaultsHelpFormatter,
                                 argparse.RawDescriptionHelpFormatter):
    """Help message formatter adding defaults to '-h' but not '--help'.

    In addition it keeps the description and epilog from being line-wrapped."""

    def _get_help_string(self, action):
        help = action.help
        if ('%(default)' not in action.help
                and '--help' not in action.option_strings):
            if action.default is not argparse.SUPPRESS:
                defaulting_nargs = [argparse.OPTIONAL, argparse.ZERO_OR_MORE]
                if action.option_strings or action.nargs in defaulting_nargs:
                    help += ' (default: %(default)s)'
        return help

class DocumentationAction(argparse._HelpAction):
    """Extended `argparse._HelpAction` to also prints the `__doc__` string."""

    def __call__(self, parser, namespace, values, option_string=None):
        print("Plug-in's short help")
        print('====================')
        print()
        parser.print_help()
        print()
        print()
        print("Plug-in's documentation")
        print('=======================')
        print()
        # `_help` is the __doc__ string. Remove indention from body, but not
        # the first line
        header, body = namespace._help.split('\n', 1)
        if header:
            print(header)
        print(argparse._textwrap.dedent(body))
        parser.exit()

class OWArgumentParser(argparse.ArgumentParser):
    """Extend `argparse.ArgumentParser` with elaborated `format_help()`.

    This class extends the `argparse.ArgumentParser` which extents the
    `format_help()` function by printing the available plug-ins whenever help
    is required.
    """

    def __init__(self, *args, interfaces={}, **kwargs):

        super().__init__(*args, **kwargs)
        self.interfaces = interfaces

    def format_help(self):

        formatter = self._get_formatter()

        # usage
        formatter.add_usage(self.usage, self._actions,
                            self._mutually_exclusive_groups)

        # description
        formatter.add_text(self.description)

        # positionals, optionals and user-defined groups
        for action_group in self._action_groups:
            formatter.start_section(action_group.title)
            formatter.add_text(action_group.description)
            formatter.add_arguments(action_group._group_actions)
            formatter.end_section()

        # plug-ins
        formatter.add_text(
                'The behaviour of the scaling method is defined '
                'by the following plug-ins:')
        formatter.start_section(None)
        for arg, p in self.interfaces.items():
            formatter._add_item(
                    self._format_plugin,
                    ['{}{:<{length}}'.format(
                            ' '*formatter._current_indent,
                            arg,
                            length=formatter._max_help_position-formatter._current_indent-1),
                     p])
        formatter.end_section()
        formatter.add_text(
                'Use "%(prog)s --<plug-in> -h" or '
                '"%(prog)s --<plug-in> --help" to get help.')

        # epilog
        formatter.add_text(self.epilog)

        # determine help from format above
        return formatter.format_help()

    def _format_plugin(self, arg, plugin):
        """Format information about plug-ins."""

        formatter = self._get_formatter()

        description_width = (
                max(formatter._width - formatter._current_indent, 11)
                - formatter._max_help_position)
        indent = ' '*formatter._max_help_position
        description = plugin.interfacename
        if len(arg) <= formatter._max_help_position-1:
            # `arg` is short enough to fit in the same line as the description
            return arg + ' ' + argparse._textwrap.fill(
                    description,
                    width=description_width,
                    subsequent_indent=indent) + '\n'
        else:
            # `arg` is too long to fit into the same line as the description
            return arg + '\n' + argparse._textwrap.fill(
                    description,
                    width=description_width,
                    initial_indent=indent,
                    subsequent_indent=indent)+'\n'


class LimitedMixin(object):
    """Mixin factory for numerical object types with lower and upper limits.

    Instances of `Limited...(LimitedMixin)` are typically passed as `type=`
    argument to the `argparse.ArgumentParser.add_argument()` method. The
    value must be min ≤ value ≤ max.
    """

    def __init__(self, typ, min, max, endpoints):
        """
        Parameters:
            typ:
                The type of the value (e.g. `int` or `float`).
            min:
                The lower limit.
            max:
                The maximum limit.
            endpoints:
                Should the endpoints be include (`'[]'` or `'()'`) or excluded
                (`']['` or `')('`) or any combination of it (e.g.
                `']]'`, `'[['`).
        """

        self.type = typ
        self.min = min
        self.max = max
        self.endpoints = endpoints
        if min is not None:
            if endpoints[0] in (']', '('):
                # Endpoint not included: min < x
                self.min_comp = typ(min).__lt__
            else:
                # Endpoint included:     min ≤ x
                self.min_comp = typ(min).__le__
        if max is not None:
            if endpoints[1] in ('[', ')'):
                # Endpoint not included: x < max
                self.max_comp = typ(max).__gt__
            else:
                # MaxEndpoint included:  x ≤ max
                self.max_comp = typ(max).__ge__

    def __call__(self, string):
        """Convert the `string` to a numeric value of type `self.type`.

        It also checks, if the value is in the interval specified during
        initialization.

        Parameters:
            string:
                A string to be converted.

        Return:
            The numeric value of the string.

        Exceptions:
            argparse.ArgumentError:
                If the value does not fall in the interval specified.
            *:
                Any exception occuring during conversion of the string.
        """

        value = self.type(string)
        msg = ''
        if (self.min is not None and not self.min_comp(value)
                or self.max is not None and not self.max_comp(value)):
            if self.min is None:
                msg = '{} is above the maximum valid value {}{}.'.format(
                        value, self.max, self.endpoints[1])
            elif self.max is None:
                msg = '{} is below the minimum valid value {}{}.'.format(
                        value, self.endpoints[0], self.min)
            else:
                msg = '{} is not in the valid range of {}{} to {}{}.'.format(
                        value,
                        self.endpoints[0], self.min,
                        self.max, self.endpoints[1])
        if msg:
            raise argparse.ArgumentError(msg)

        return value

    def __repr__(self):
        return "{}(min={}, max={}, endpoints='{}')".format(
                type(self).__name__, self.min, self.max, self.endpoints)

class LimitedInt(LimitedMixin):
    """Factory for creating `int` objects with mininmum and maximum limits.

    Instances of `LimitedInt` are typically passed as `type=` arguments to
    the `argparse.ArgumentParser.add_argument()` method. The value must be
    min ≤ value ≤ max.
    """

    def __init__(self, min=None, max=None, endpoints='[]'):
        """
        Parameters:
            min=None:
                The lower limit.
            max=None:
                The maximum limit.
            endpoints='[]':
                Should the endpoints be include ('[]' or '()') or excluded
                ('][' or ')(') or any combination of it (e.g. ']]', '[[').
        """
        super().__init__(int, min, max, endpoints)

class LimitedFloat(LimitedMixin):
    """Factory for creating `float` objects with mininmum and maximum limits.

    Instances of `LimitedFloat` are typically passed as `type=` arguments to
    the `argparse.ArgumentParser.add_argument()` method. The value must be
    min ≤ value ≤ max.
    """

    def __init__(self, min=None, max=None, endpoints='[]'):
        """
        Parameters:
            min=None:
                The lower limit.
            max=None:
                The maximum limit.
            endpoints='[]':
                Should the endpoints be include ('[]' or '()') or excluded
                ('][' or ')(') or any combination of it (e.g. ']]', '[[').
        """
        super().__init__(float, min, max, endpoints)


# = Helper for writing output files ===========================================

class FortranPrinter(object):
    """Convenience class to save us some typing.

    Usage:
        >>> filename = 'fortran.out'
        >>> with open(filename) as f:
        >>>     p = FortranPrinter(f)
        >>>     p('{:<20};', number)
        >>>     p('{o.number:<20};', o=myobj)
    """

    def __init__(self, file):
        """
        Parameters:
            file:
                The file to print to.
        """
        if file is None:
            self.print_kwargs = {}
        else:
            self.print_kwargs = {'file': file}

    def __call__(self, msg='', *args, **kwargs):
        """
        Parameters:
            msg:
                Either an object with a 'format' method (such as a string), in
                which case it prints 'msg.format(*args)'; otherwise it prints
                'msg, *args'.
            *args:
                See the 'msg' parameter.
            **kwargs:
                Passed on to the print function.
        """

        if hasattr(msg, 'format'):
            print(msg.format(*args), **kwargs, **self.print_kwargs)
        else:
            print(msg, *args, **kwargs, **self.print_kwargs)


# = Helper for fitting curves to data points ==================================

class RangeCheckMixin(object):
    """Mixin factory to check if value is within lower and upper limits.

    A couple of functions, such as approximation or interpolation functions
    of numpy and scipy, can be called with values outside of the range used
    to build the approximation/interpolation. This can be fine, but sometimes
    we want to make sure, that we do not venture into unexplored territory, so
    we have to check, if the value when calling this function is inside the
    range.

    This mixin class stores the minima and maxima of the range and a default
    value, if the range check should be done.

    Usage:
    =====

    Inherit your class from this and call
        `RangeCheckMixin.__init__(self, x_data, strict)`
    during initialization (`__init__()`) of your class.

    You overload the `__call__()` method and call
        `RangeCheckMixin.__call__(self, val, strict)`
    before you call your evaluating function:
        ```
        def __call__(self, val, strict=None):
            try:
                RangeCheckMixin.__call__(self, val, strict)
            except ValueError as e:
                raise e
            else:
                return <inherited_approx/interpol_class>.__call__(self, val)
        ```
    """

    @property
    def min(self):
        return self._min

    @property
    def max(self):
        return self._max

    def __init__(self, x_data, strict=True):
        """
        Parameters:
            x_data:
                Specifies the lower and upper limit as `min(x_data)` and
                `max(x_data)`, respectively.
            strict=True:
                If the range check should be done
        """

        # Save `min`, `max` and `strict` into the namespace.
        # We do it the complicated way, because sometimes the easy way
        #   `self._min = val`
        # does not work.
        self.__dict__['_min'] = min(x_data)
        self.__dict__['_max'] = max(x_data)
        self.__dict__['_strict'] = strict

    def __call__(self, val, strict):
        """Check if `val` is in the given interval.

        Parameters:
            val:
                Value to be checked. If the value is an iterable, such as a
                `numpy` array, we check, if any value in this collection is
                inside the interval.
            strict:
                If `True`, checks are performed.

        Exceptions:
            ValueError:
                If `val` is not in the interval.
        """

        _strict = strict if strict is not None else self._strict
        if isinstance(val, collections.Iterable):
            _isininterval = (self._min <= min(val) and max(val) <= self._max)
        else:
            _isininterval = (self._min <= val <= self._max)

        if (_strict and _isininterval) or not _strict:
            return True
        else:
            raise ValueError(
                "{} not in valid interval [{}, {}]".format(
                    val, self._min, self._max))


class PolyFit(np.poly1d, RangeCheckMixin):
    """Fit polynomials of degree 'deg' to x-y data.

    Implements the `numpy.poly1d` one-dimensional polynomial class with a
    least squares polynomial fit (`numpy.polyfit`) during initialisation and
    an optional range check when called.

    See:
    - https://docs.scipy.org/doc/numpy/reference/generated/numpy.poly1d.html
    - https://docs.scipy.org/doc/numpy/reference/generated/numpy.polyfit.html
    """

    def __init__(self, x_data, y_data, deg=4, strict=True, **kw):
        """
        Parameters:
            x_data:
                Iterable with x-data.
            y_data:
                Iterable with y-data.
            deg=4:
                The degree of the polynomial to be fitted.
            strict=`True`:
                If `True`, the range will be checked during evaluation (see
                also `RangeCheckMixin` class).
        """

        np.poly1d.__init__(
            self,
            np.polyfit(x_data, y_data, deg, **kw),
            **kw)
        RangeCheckMixin.__init__(self, x_data, strict)

    def __call__(self, x, strict=None):
        """Return the y(x) value with optional range checking of `x`.

        Parameters:
            x:
                The `x` value.
            strict=None:
                See `RangeCheckMixin.__call__()`.

        Return:
            The value y(x).

        Exceptions:
            ValueError:
                If the value `x` is not in the specified range.
        """

        try:
            RangeCheckMixin.__call__(self, x, strict)
        except ValueError as e:
            raise e
        else:
            return np.poly1d.__call__(self, x)


class ApproxFit(scipy.interpolate.Rbf, RangeCheckMixin):
    """Fit radial basis function approximation/interpolation to x-y data.

    Implements the `scipy.interpolate.Rbf` radial basis function approximation/
    interpolation class with optional range check when called.

    See:
    - https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.Rbf.html#scipy.interpolate.Rbf
    """

    def __init__(self, x_data, y_data, strict=True, **kw):
        """
        Parameters:
            x_data:
                Iterable with x-data.
            y_data:
                Iterable with y-data.
            strict=`True`:
                If `True`, the range will be checked during evaluation (see
                also `RangeCheckMixin` class).
        """

        scipy.interpolate.Rbf.__init__(self, x_data, y_data, **kw)
        RangeCheckMixin.__init__(self, x_data, strict)

    def __call__(self, val, strict=None):
        """Return the y(x) value with optinal range checking of `x`.

        Parameters:
            x:
                The `x` value.
            strict=None:
                See `RangeCheckMixin.__call__()`.

        Return:
            The value y(x).

        Exceptions:
            ValueError:
                If the value `x` is not in the specified range.
        """

        try:
            RangeCheckMixin.__call__(self, val, strict)
        except ValueError as e:
            raise e
        else:
            return scipy.interpolate.Rbf.__call__(self, val)

class Spline(scipy.interpolate.InterpolatedUnivariateSpline, RangeCheckMixin):
    """Fit a one-dimensional interpolating spline to x-y data.

    Implements the `scipy.interpolate.InterpolatedUnivariateSpline` class with
    optional range check when called.

    FIXME:
        Note that none of the other methods, such as `derivative()` are checked
        for the range!

    See:
    - https://docs.scipy.org/doc/scipy-0.15.1/reference/generated/scipy.interpolate.InterpolatedUnivariateSpline.html#scipy.interpolate.InterpolatedUnivariateSpline
    """

    def __init__(self, x_data, y_data, strict=True, **kw):
        """
        Parameters:
            x_data:
                Iterable with x-data.
            y_data:
                Iterable with y-data.
            strict=`True`:
                If `True`, the range will be checked during evaluation (see
                also `RangeCheckMixin` class).
        """

        scipy.interpolate.InterpolatedUnivariateSpline.__init__(
                self, x_data, y_data, **kw)
        RangeCheckMixin.__init__(self, x_data, strict)

    def __call__(self, val, strict=None):
        """Return the y(x) value with optinal range checking of `x`.

        Parameters:
            x:
                The `x` value.
            strict=None:
                See `RangeCheckMixin.__call__()`.

        Return:
            The value y(x).

        Exceptions:
            ValueError:
                If the value `x` is not in the specified range.
        """

        try:
            RangeCheckMixin.__call__(self, val, strict)
        except ValueError as e:
            raise e
        else:
            return scipy.interpolate.InterpolatedUnivariateSpline.__call__(
                    self, val)
