#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plug-in framwork used by PyOWscaling.

If you want to program a plug-in, look at the `plugins.py` file, where you can
find a description.

How this plug-in framework works is described in these sources:
    * https://yannik520.github.io/python_plugin_framework.html
    * http://martyalchin.com/2008/jan/10/simple-plugin-framework/
    * https://github.com/regisd/simple_plugin_framework
It is also heavily based on these code examples.

Public classes:
    PluginMount:
        Plug-in meta class.
    IPlugin:
        All plug-ins should derive from this interface class.
    PluginManager:
        Plug-in manager as a singleton.


Created on Thu Jan 26 14:34:19 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__version__ = '3.1.0'


import sys
import os.path
import importlib
from collections import OrderedDict


class PluginMount(type):
    """Plug-in meta class.

    * A way to declare a mount point for plug-ins. Since plug-ins are an
      example of loose coupling, there needs to be a neutral location,
      somewhere between the plug-ins and the code that uses them, that each
      side of the system can look at, without having to know the details of
      the other side.
    * A way to register a plug-in at a particular mount point. Since internal
      code don't want to look around to find plug-ins that might work for it,
      there needs to be a way for plug-ins to announce their presence. This
      allows the guts of the system to be blissfully ignorant of where the
      plug-ins come from; again, it only needs to care about the mount point.
    * A way to retrieve the plug-ins that have been registered. Once the
      plug-ins have done their thing at the mount point, the rest of the
      system needs to be able to iterate over the installed plug-ins and use
      them according to its need.

    Add the parameter `metaclass=MountPoint` in any class to make it a mount
    point.
    """

    def __init__(cls, name, bases, attrs):
        if not hasattr(cls, 'plugins'):
            # This branch only executes, when processing the mount point
            # itself. So, since this is a new plug-in type, not an
            # implementation, this class shouldn't be registered as a plug-in.
            # Instead, it sets up a dictionary, where plug-ins can be
            # registered later.
            cls.plugins = {}

        else:
            # This must be a plug-in implementation, which should be
            # registered. Simply appending it to the dictionary is all that's
            # needed to keep track of it later.
            cls.plugins[attrs['name']] = cls

            # Save the filename of the plug-in implementation.
            # History:
            # We cannot use `__file__` here, because this would return the
            # filename of THIS file, but we want the file name of the
            # plug-in's class inheriting this metaclass.
            # The two lines of code originates from this answer on
            # stackoverflow.com:
            # https://stackoverflow.com/questions/6810999/how-to-determine-file-function-and-line-number#answer-6811020
            # The code presented there can be condensed to:
            #   cls.__filename__ = inspect.stack()[1].filename
            # This needs the `inspect` module imported. To ease the load on
            # the system, the `inspect` module was inspected and the calls
            # substituted repeatedly and weeded out. The code could be boiled
            # down to these two lines.
            # One should mention, that `sys._getframe(1)` gets the second
            # frame object below the top of the stack (see
            # `help(sys._getframe)`). Since the top of the stack is the call
            # to this method, the next element is the plug-in's `__init__()`
            # method. Et voilà...
            plugin_frame = sys._getframe(1)
            cls.__filename__ = plugin_frame.f_code.co_filename

            # Update class's `arguments` dictionary with the `arguments`
            # dictionaries from all derived classes.
            # See:
            # https://stackoverflow.com/questions/8647370/python-update-inherited-class-level-dictionary#answer-42036304
            # Which uses
            #   def __new__(cls, name, bases, attrs):
            #       new_class = super(CheckerMeta, cls).__new__(
            #               cls, name, bases, attrs)
            #       base_args = [bc.arguments
            #                    for bc in bases
            #                    if hasattr(bc, 'arguments')]
            #       args = base_args + [getattr(new_class, 'arguments', None)]
            #       new_class.arguments = {}
            #       for arg in args:
            #           new_class.arguments.update(arg)
            #       return new_class
            # Note that this answer uses `__new__()` whereas we do this magic
            # in `__init__()`. We get the `arguments` from *all* inherited
            # classes with `.mro()`. We also have to do some magic to use the
            # annotation from the wrapped class (if it's wrapped).
            args = OrderedDict()
            for bc in reversed(cls.mro()):
                try:
                    # Get the `arguments` function, possibly wrapped
                    arguments = bc.arguments
                    try:
                        arguments = arguments.__wrapped__
                    except AttributeError:
                        pass
                except AttributeError:
                    continue

                code = arguments.__code__
                for i, var in enumerate(code.co_varnames):
                    # Positional and keyword argument
                    try:
                        annotation = arguments.__annotations__[var]
                    except KeyError:
                        continue
                    # Default value for the keyword argument
                    if code.co_argcount - i > 0 and arguments.__defaults__:
                        default = arguments.__defaults__[
                                i
                                + len(arguments.__defaults__)
                                - code.co_argcount]
                        if default is not None:
                            annotation['default'] = default
                    # Store it
                    args[var] = annotation

            cls.arguments = args

    def print_plugins(cls):
        """Print a list of plug-ins found."""
        format_str = '{:>8}.  {:<24} {:<7} : {}'
        if cls.plugins:
            print(format_str.format('No', 'Name', 'Version', 'Class'))
            for i, (name, kls) in enumerate(cls.plugins.items()):
                print(format_str.format(i, name, kls.__version__, kls))
        else:
            print(format_str.format('None', '', ''))

    def get_plugins(cls):
        """Return the dictionary with all plug-ins."""
        return cls.plugins


class IPlugin(metaclass=PluginMount):
    """All plug-ins should derive from this interface class.

    A plug-in implementing the `IPlugin` interface would look like this:

        ```
        class MyPlugin(IPlugin):
            '''My plug-in of the IPlugin type.'''
            pass
        ```

    `PluginMgr().get_plugins()` returns by default all plug-ins, which derive
    from `IPlugin`. It can get other plug-ins as follows:

        ```
        class IPlugin2(metaclass=PluginMount):
            pass

        class MyPlugin2(IPlugin2):
            pass

        print(PluginMgr().get_plugins(IPlugin2))
        ```

    Note:
        The `IPlugin` interface class is just a convenience for lazy
        programmers, who cannot be bothered to implement it on their own. It is
        highly encouraged to implement your own class(es).
    """
    pass

class PluginManager(object):
    """Plug-in manager as a singleton.

    A typical usage scenario is:

        ```
        import pluginframework

        pluginmgr = pluginframework.PluginManager(
                plugin_dirs=['./plugins/', '/usr/local/share/plugins/'])
        pluginmgr.collect_plugins()

        plugins = pluginframework.IPlugin.get_plugins()
        # Or:
        # plugins = pluginmgr.get_plugins(interface=pluginframework.IPlugin)
        print('Available scaling methods:')
        pluginframework.IPlugin.print_plugins()
        # Or:
        # pluginmgr.print_plugins(interface=pluginframework.IPlugin)
        ```
    """

    __instance = None

    def __new__(cls, *args, **kwargs):
        """Make the manager class as singleton.

        See:
            https://python-3-patterns-idioms-test.readthedocs.io/en/latest/Singleton.html
        """

        if cls.__instance is None:
            cls.__instance = super().__new__(cls)

        return cls.__instance

    def __init__(self, plugin_dirs=['./plugins/'], default_interface=IPlugin):
        """
        Parameters:
            plugin_dirs=`['./plugins/']`:
                Iterable with directories to all plug-ins.
            default_interface=`IPlugin`:
                Interface class which members should be returned by
                `get_plugins()`, if the keyword argument `interface` is not
                given.
        """

        self.plugin_dirs = {pdir: False for pdir in plugin_dirs}
        self.default_interface = default_interface

    def import_plugin(self, filepath):
        """Import the Python file `filepath`.

        Parameters:
            filepath:
                Path to Python file to be imported.

        See:
            https://stackoverflow.com/questions/67631/how-to-import-a-module-given-the-full-path#answer-67692
        """

        name = filepath[:-3].replace(os.path.sep, '.').replace('..', '.', 1)
        try:
            # Requires Python 3.6+!
            spec = importlib.util.spec_from_file_location(name, filepath)
            foo = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(foo)
        except AttributeError:
            # Python 3.3, 3.4 & 3.5
            loader = importlib.machinery.SourceFileLoader(name, filepath)
            foo = loader.load_module()

    def collect_plugins(self):
        """Collect all plug-ins in the specified plug-in directories."""

        for pdir, loaded in self.plugin_dirs.items():
            if not loaded:
                # Requires Python 3.5+
                for entry in os.scandir(pdir):
                    if entry.name.endswith('.py'):
                        self.import_plugin(os.path.join(pdir, entry.name))

    def print_plugins(self, interface=None):
        """Print a list of all plug-ins of type `interface`.

        This is a convenience function and the same result can be achieved by:

            ```
            <interface>.print_plugins()
            ```

        Parameters:
            interface=`IPlugins`:
                Interface class which classes should be returned.
        """

        if interface is None:
            self.default_interface.print_plugins()
        else:
            interface.print_plugins()

    def get_plugins(self, interface=None):
        """Return all plug-ins of type `interface`.

        This is a convenience function and the same result can be achieved by:

            ```
            <interface>.get_plugins()
            ```

        Parameters:
            interface=`IPlugins`:
                Interface class which classes should be returned.

        Return:
            A dictionary with name:class pairs.
        """

        if interface is None:
            return self.default_interface.get_plugins()
        else:
            return interface.get_plugins()
