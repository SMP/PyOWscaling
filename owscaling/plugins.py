#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Collection of interface classes used to write plug-ins for PyOWscaling.

Available interfaces classes:
    IFrictionlinePlugin:
        To calculate the frictional drag coefficient of a section based on the
        Reynolds number of the section.
    IFormfactorPlugin:
        To calculate the form factor of a section based on the thickness to
        chord ratio.
    IDensityPlugin:
        To calculate the density of water for open-water, self-propulsion and
        full scale condition based on the water temperature.
    IViscosityPlugin:
        To calculate the viscosity of water for open-water, self-propulsion
        and full scale condition based on the water temperature.
    IPropellergeometryPlugin:
        For reading hand holding the propeller geometry.
    IOWReaderPlugin:
        For reading and holding the open-water characteristics.
    IScalingPlugin:
        The main working horse implementing the scaling procedure.
    IOWWriterPlugin:
        For writing the scaled data into output files.


Created on Sun Jan 22 20:25:53 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__version__ = '3.1.0'


import os
import argparse
from numbers import Number
from collections import OrderedDict

import numpy as np
import scipy.integrate

from . import pluginframework
from . import helpers


pi = np.pi
sqrt = np.sqrt


import inspect
from functools import wraps

def autoargs(f):
    """
    Decorator to automatically save the (keyword) arguments into `self.args`.

    This decorator can (and should) be used, to automatically save the
    arguments into the `argparse.Namespace` `self.args`. The intended usage is
    to decorate the `arguments()` function of your plug-in with it.

    >>> class MyPlugin(IViscosityPlugin):
    ...     @autoargs
    ...     def arguments(self, nu, temperature=18, *args, **kwargs):
    ...         super().arguments(*args, **kwargs)
    >>> p = MyPlugin(1e-6)
    >>> p.args
    Namespace(nu=1e-6, temperature=18)

    Source:
        https://stackoverflow.com/questions/1389180/python-automatically-initialize-instance-variables#answer-1389216
    """
    # The orignal code uses `inspect.getargspec(f)`, but this is deprecated
    # since version 3.0.
    # `inspect.getfullargspec(f)` returns:
    #   args, varargs, varkw, defaults, kwonlyargs, kwonlydefaults, annotations
    names, _, _, defaults, _, _, _ = inspect.getfullargspec(f)

    @wraps(f)
    def wrapper(self, *args, **kwargs):
        # Positional and keyword arguments
        for name, arg in list(zip(names[1:], args)) + list(kwargs.items()):
            if not name.startswith('_'):
                setattr(self.args, name, arg)

        # Default values for keyword arguments
        for name, default in zip(reversed(names), reversed(defaults or ())):
            if not hasattr(self.args, name) and not name.startswith('_'):
                setattr(self.args, name, default)

        f(self, *args, **kwargs)

    return wrapper


###############################################################################
#
# Mix-in interface classes
#

class _IPlugin(object):
    """Basic interface class to be derived by all plug-in interfaces.

    The derived plug-in _interface_ must set the class attribute
    `interfacename`.

    This interface class provides two APIs for its initialization:
        1. The typical Python-way with positional and keyword arguments.
        2. A parsing of the special `argv` keyword argument with the help of
           the `argparse` package such as it were a command line.
    See below for an explanation.

    The derived plug-ins **must** set the class attributes:
        * `name`,
        * `__version__`,
        * `__copyright__`
    and the class method:
        * `__call__()`.
    They **can** override the class methods:
        * `arguments()`,
        * `__str__()` and
        * `__init__()`.

    Properties:
        interfacename:
            The derived plug-in _interface_ **must** set the class attribute
            `interfacename`.
            Format: A string.
        name:
            The derived plug_in **must** set this class attribute.
            Format: Any string, but consider avoiding spaces and fancy
            characters, because this name is used to select the plug-in at
            the command line.
        __version__:
            The derived plug-in **must** set this class attribute.
            Format: It should have the format `'0.0.0'`.
        __copyright__:
            The derived plug-in **must** set this class attribute.
            Format: It should have the format
            `'Copyright (C) {year}  {name of author}'`.
        ArgumentParser:
            The read-only `argparser.ArgumentParser` used to parse the
            arguments with `parse_args()`.

    Methods:
        __call__(*args, **kwargs):
            The derived plug-ins **must** definitely override the `__call__()`
            method implementing its core functionality.
        arguments(..., *args, **kwargs):
            This method defines both initialization APIs. This magic is
            accomplished by annotations and explained below.
        __str__():
            The derived interfaces and plug-ins can (and possibly should)
            override the `__str__()` method to give a better representation
            of the plug-in when printing. It should append the new information
            to the existing output. For an example see `_IPlugin.__str__()`.
        __init__(*args, **kwargs):
            If the derived class wants to save some values in class attributes,
            this should be done here. But first it must pass `*args` and
            `**kwargs` to the parent class, after which the instance attribute
            `self.args` becomes available, which contains all arguments in an
            `argparse.Namespace` object.
            If there is a `argv` keyword, it calls `parse_args(argv)`, which
            does a command line parsing, otherwise it calls `arguments(*args,
            **kwargs)`, which stores the positional and keyword arguments.
            Most of the time there is no need for the plug-in to override
            it. If you do it, you must make sure that the parent's `__init__()`
            method is called!

    Initialization
    ==============

    This plug-in interface class provides two possible ways to initialize the
    plug-in:
        1. The classical Python-way
        2. An API which is compatible with the
           `argparse.ArgumentParser.parse_args()` method and which can be used
           for command line programs.

    Both APIs are provided by the `arguments()` method. The `__init__()`
    method decides which path is taken:
        * If it is called with a non-`None` keyword argument `argv`, its value
          is considered a list of arguments to be parsed by the
          `argparse.ArgumentParser.parse_args()` method and it is passed on to
          the `__parse_args()` method. This method parses the list and store
          the results in the `self.args` attribute.
        * If the `argv` keyword argument is `None` (the default), the
          `arguments()` method is actually called with the positional and
          keyword parameters from the call to `__init__()` with the aim to
          store the values in the `self.args` attribute.

    Either way, the arguments become available in the `self.args` attribute.
    This is an `argparse.Namespace` object and the arguments are available
    in the dot notation (see below).

    The classic Python-way
    ----------------------

    There is nothing special about this: All arguments, either positional or
    keywords, are given in the parameter list of `arguments()` instead of
    `__init__()`. Hence instead of writing:
        >>>    def __init__(self, nu, temperature=18, *args, **kwargs):
        >>>        super().__init__(*args, **kwargs)
    the `__init__()` method will be omitted and replace by `arguments`:
        >>>    def arguments(self, nu, temperature=18, *args, **kwargs):
        >>>        super().arguments(*args, **kwargs)

    This does not save the parameters into `self.args` yet. To accomplish this
    task, simple decorate `arguments()` with `@autoargs`:
        >>>    @autoargs
        >>>    def arguments(self, nu, temperature=18, *args, **kwargs):
        >>>        super().arguments(*args, **kwargs)

    The `ArgumentParser` compatible way
    -----------------------------------

    To help command line programs to use the plug-ins, it must be possible to
    define the command line arguments in a generic way. The interface class
    uses annotations to accomplish this task: Each positional and keyword
    argument must have a annotation describing the parameter:
        >>>    @autoargs
        >>>    def arguments(
        >>>          self,
        >>>          nu: {
        >>>              'type': LimitedFloat(min=0),
        >>>              'help': 'constant value of kinematic viscosity [m²/s]'
        >>>              },
        >>>          temperature: {
        >>>              'args': ('-t', '--temperature'),
        >>>              'type': float,
        >>>              'help': 'temperature of the water [°C]',
        >>>              'metavar': 'TEMP'
        >>>              } = 18,
        >>>          *args, **kwargs):
        >>>        super().arguments(*args, **kwargs)
    The annotation for each parameter is a dictionary with elements
    corresponding to `argparse.ArgumentParser.add_argument()` (see there).
    Note that the keys `'dest'` and `'default'` are overwritten, because the
    destination is given by the argument's name and the default by the
    keyword's default value.
    Note also that if the argument is a keyword argument, you need to specify
    the `'args'` key.

    The `self.args` attribute
    -------------------------

    After initialization, the parameters are saved in the instance attribute
    `self.args`. This is an `argparse.Namespace` object. As such the parameters
    are accessed in the "dot" notation, e.g. the parameter `nu` can be accessed
    as `self.args.nu`.

    Examples
    --------

    Using the above declaration, these two calls to initialize the plug-in
    `MyPlugin` result in the same outcome:
        >>> "Classic" Python arguments
        >>> myplugin = MyPlugin(1e-6, temperature=20)
        >>> myplugin.args
        Namespace(nu=1e-6, temperature=20)
        >>> # "Command line" arguments
        >>> myplugin = MyPlugin(argv=['1e-6' '--temperature', '20'])
        >>> myplugin.args
        Namespace(nu=1e-6, temperature=20)
    """

    @property
    def ArgumentParser(self):
        """The `arparse.ArgumentParser` to be used to parse arguments."""
        if not hasattr(self, '_parser'):
            # Initialize the parser
            self._parser = argparse.ArgumentParser(
                    prog='--<plug-in "{self.interfacename}"> {self.name}'.format(self=self),
                    description='Help for {} plug-in "{}":'.format(
                            self.interfacename, self.name),
                    epilog=self.__copyright__,
                    formatter_class=helpers.ArgumentDefaultsHFormatter,
                    add_help=False)
        return self._parser

    def __init__(self, *args, argv=None, **kwargs):
        """
        Parameters:
            argv=None:
                A list of arguments to be parsed by
                `argparse.ArgumentParser.parse_args()` if not `None`.
            *args:
                Positonal arguments as defined by `arguments()` through
                inheritance.
            **kwargs:
                Keyword arguments as defined by `arguments()` through
                inheritance.

        """
        if argv is not None:
            self.__parse_args(argv)
        else:
            self.args = argparse.Namespace()
            self.arguments(*args, **kwargs)

    @autoargs
    def arguments(
            self,
            _h: {
               'args': '-h',
               'action': 'help',
               'help': 'show short help message and exit'
               } = None,
            _help: {
               'args': '--help',
               'default': '{self.__doc__}',
               'action': helpers.DocumentationAction,
               'help': 'show short help and extended documentation and exit'
               } = None,
            _version: {
               'args': ('-V', '--version'),
               'action': 'version',
               'version': '{self.interfacename} plug-in "{self.name}", '
                       'v{self.__version__}',
               'help': "show plug-in's version number and exit"
               } = None,
            debug: {
               'args': ('-d', '--debug'),
               'action': 'store_true',
               'help': 'debug the {self.interfacename}'
               } = False
            ):
        """General arguments for all plug-ins."""
        pass


    def __parse_args(self, argv):
        """Parse the command line like arguments into `self.args`.

        `argv` is a list of strings as supplied on the command line. This
        string is parsed according to the options given in `self.arguments`
        and the result is stored in `self.args` of the type
        `argparse.Namespace`.

        Parameters:
            argv:
                A list of strings as supplied on the command line to be parsed
                according to `self.arguments`.
        """

        for dest, arguments in self.arguments.items():

            # Our dictionary of arguments for `ArgumentParser.add_argument()`.
            kwargs = OrderedDict()
            # Our positional arguments for `ArgumentParser.add_argument()`.
            options = (dest, )
            for k, v in arguments.items():

                if k == 'args':
                    # An optional argument:
                    # The names of the optional arguments are stored under the
                    # 'args' keyword. We save them under options and store the
                    # name of the destination under 'dest' in our dictionary.
                    # of arguments for `ArgumentParser.add_argument()`.
                    kwargs['dest'] = dest
                    if isinstance(v, str):
                        # We allowed single string, so ensure it's a tuple.
                        options = (v, )
                    else:
                        options = v

                else:
                    # Some values for `add_argument()` can be strings and can
                    # contain formatting directives containing `{self}`. We
                    # must format them before adding them to our dictionary
                    # of arguments for `ArgumentParser.add_argument()`.
                    if isinstance(v, str):
                        kwargs[k] = v.format(self=self)
                    else:
                        kwargs[k] = v

            try:
                self.ArgumentParser.add_argument(*options, **kwargs)
            except ValueError as e:
                raise ValueError(
                        "arguments['{}'] in plug-in '{}' (class '{}'): "
                        "{}".format(
                                dest, self.name, self.__class__.__name__, e))

        self.args = self.ArgumentParser.parse_args(args=argv)

    def __str__(self):
        """Return string representation of the class.

        Extend it in the interface and plug-ins like:
            ```
            return (super().__str__() +
            '''Info:       {info}
            More info:  {self.more_info}
            '''.format(info='Information', self=self))
            ```
        """

        return """{interfacename}:
        Class:      {self.__class__}
        Name:       {self.name}
        Version:    {self.__version__}
        Sourcefile: {self.__filename__}
        Args:       {self.args.__dict__}
        """.format(interfacename=self.interfacename.capitalize(),
                   self=self)

    def __call__(self, *args, **kwargs):
        """Return the value specific to the plug-in.

        Overwrite it in your plug-in!

        Return:
            The value specific to the plug-in.
        """

        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must implement the "__call__()" method.'.format(
                        self.name, self.__class__.__name__))


class _IFile(_IPlugin):
    """Basic interface class for plug-in interfaces handling files.

    The derived interface classes must extend the ArgumentParser property by
    a 'file' argument, which is stored in the `self.args.file` attribute by
    the `__init__()` method. For examples look into `_IFilereader` (and its
    derived classes `IPropellergeometryPlugin` and `IOWReaderPlugin`) or
    `_IFilewriter` (and its derived classes `HSVAcsv` and `HSVAypp`). Note
    that we cannot add the 'file' argument straight here in this mix-in class,
    because we want to have different help strings for different derived
    interfaces.

    Methods:
        get_filename(f):
            Get the file name of `f` or `self.args.file`.
        get_fileencoding(f):
            Get the file encoding of `f` or `self.args.file`.
    """

    def __str__(self):
        return (super().__str__() +
        """File:       {filename}
        Encoding:   {fileencoding}
        """.format(filename=self.get_filename(),
                fileencoding=self.get_fileencoding(),
                self=self))

    def __call__(self, *args, **kwargs):
        raise NotImplementedError(
                'To handle files with plug-in "{}" (class: {}) '
                'use the specialised method.'.format(
                        self.name, self.__class__.__name__))

    def get_filename(self, f=None):
        """Return the file name of `f` or `self.args.file`.

        Parameters:
            f=None:
                A file-like object or string. If `None`, `self.args.file` is
                used.

        Return:
            The file name or `None`, if `f` is not a string and the file-like
            object has no `name` attribute.
        """

        if f is None:
            f = self.args.file

        if isinstance(f, str):  # Check if we got a string
            return f
        else:
            try:
                return f.name
            except AttributeError:
                return None

    def get_fileencoding(self, f=None):
        """Return the file encoding of `f` or `self.args.file`.

        Parameters:
            f:
                A file-like object or string. If `None`, `self.args.file` is
                used.

        Return:
            The file encoding, `"n/a"` if `f` is a string or `None`, if the
            file-like object has no `encoding` attribute.
        """

        if f is None:
            f = self.args.file

        if isinstance(f, str):  # Check if we got a string
            return 'n/a'
        else:
            try:
                return f.encoding
            except AttributeError:
                return None


class _IFilereader(_IFile):
    """Basic interface class for plug-in interfaces reading files.

    See also parent class `_IFile`.

    Notes on implementation of plug-ins deriving interface classes which are
    derived with this mix-in, e.g. `IPropellergeometryPlugin` and
    `IOWReaderPlugin`:

        * The main program calls the `read()` method to read the file.
        * `read()` opens the file and reads its contents with `readlines()`.
          It calls `_read()` with the file contents as argument.
        * There are different ways to use this interface class:
            1. Override the `_read()` method.
               The traditional way is, that this class's `read()` method reads
               the file with `readlines()` into a list. A subsequent call to
               `_read()` (with the file data as argument) puts this data into
               something useable.
            2. Override the `read()` method to do whatever you want.

    Methods:
        read():
            Read the input file.
    """

    def __call__(self, *args, **kwargs):
        raise NotImplementedError(
                'To read files with plug-in "{}" (class: {}) '
                'use the "read()" method.'.format(
                        self.name, self.__class__.__name__))

    def read(self):
        """Read the input file `self.args.file`.

        The input `self.args.file` can be a filename, a file object or any
        file-like object.

        The actual reading functionality must be implemented by the user as
        outlined in the class's documentation.
        """

        # Read all lines of the file (we can do it that way without a loop,
        # because the file is not very big).
        if isinstance(self.args.file, str):     # Check if we got a string
            with open(self.args.file, 'r') as f:
                filedata = f.readlines()
        else:
            filedata = self.args.file.readlines()

        filedata = [s.rstrip() for s in filedata]

        self._read(filedata)

    def _read(self, filedata):
        """Convert the `filedata` into something useable.

        Parameters:
            filedata:
                An iterable with one line per item.

        Must be implemented by the child class.
        """
        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must implement the "_read()" method.'.format(
                        self.name, self.__class__.__name__))


class _IFilewriter(_IFile):
    """Basic interface class for plug-in interfaces writing files.

    See also parent class `_IFile`.

    Notes on implementation of plug-ins deriving interface classes which are
    derived with this mix-in, e.g. `IOWWriterPlugin`:

        * The main program calls `inititalize()` with the data to be saved.
        * The main program calls the `write()` method to write the file.
        * `write()` opens the file and writes the data to it by calling
          `_write()` with the opened file as argument.
        * There are different ways to use this interface class:
            1. Override the `_write()` method.
               The traditional way is, that this class's `write()` method opens
               the file and calls `_write()`, which writes the data to the
               file.
               Data needed by `_write()` should be saved by an earlier call to
               `initialize()`.
            2. Override the `write()` method to do whatever you want.

    Methods:
        write(f):
            Write to the output file.
    """

    def __call__(self, *args, **kwargs):
        raise NotImplementedError(
                'To write files with plug-in "{}" (class: {}) '
                'use the "write()" method.'.format(
                        self.name, self.__class__.__name__))

    def write(self):
        """Write to the output file `self.args.file`.

        The output can be a filename, a file object or any file-like object.

        The actual writing functionality must be implemented by the user as
        outlined in the class's documentation.
        """

        # Open the file and write to it
        if isinstance(self.args.file, str):     # Check if we got a string
            with open(self.args.file, 'w') as f:
                self._write(f)
        else:
            with self.args.file as f:
                self._write(f)

    def _write(self, f):
        """Write data to the already opened file `f`.

        Must be implemented by the child class.

        Parameters:
            f:
                An open file or file-like object.
        """
        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must implement the "_write()" method.'.format(
                        self.name, self.__class__.__name__))


###############################################################################
#
# Interface classes
#

class IFrictionlinePlugin(_IPlugin, metaclass=pluginframework.PluginMount):
    """Interface for plug-ins implementing a friction line."""
    interfacename = 'friction line'


class IFormfactorPlugin(_IPlugin, metaclass=pluginframework.PluginMount):
    """Interface for plug-ins implementing a section form factor."""
    interfacename = 'form factor'


class IDensityPlugin(_IPlugin, metaclass=pluginframework.PluginMount):
    """Interface for plug-ins implementing a water density."""
    interfacename = 'density'


class IViscosityPlugin(_IPlugin, metaclass=pluginframework.PluginMount):
    """Interface for plug-ins implementing a water viscosity."""
    interfacename = 'viscosity'


class IScalingPlugin(_IPlugin, metaclass=pluginframework.PluginMount):
    """Interface for plug-ins implementing a scaling method.

    Notes on implementation of plug-ins deriving from this interface class:

        * The main program calls `inititalize()` with the (initialized)
          plug-ins needed by the scaling method.
        * The main program calls the `scale()` method with the appropriate
          arguments to scale the open-water characteristics.
        * `scale()` scales the data by calling `_scale()`, which returns a
          dictionary of files written by the scaling method (see "External
          programs" below and `_scale()`).
        * There are different ways to use this interface class:
            1. Override the `_scale()` method.
            2. Override the `scale()` method to do whatever you want.
            3. Override the `initialize()` method to store the data to be
               saved.

    Methods:
        KT(JEI, condition):
            Calculate the thrust coefficient KT(J) for `condition`.
        KQ(JEI, condition):
            Calculate the torque coefficient KQ(J) for `condition`.
        ETA(JEI, condition):
            Calculate the efficiency η(J) for `condition`.
        CF_ow(*args, **kwargs):
            Calculate friction coefficient of the blade in open-water condition.
        CF_sp(*args, **kwargs):
            Calculate friction coefficient of the blade in self-propulsion condition.
        CF_fs(*args, **kwargs):
            Calculate friction coefficient of the blade in full-scale condition.
        CF(condition, RN, XE, DP, JEI=None, **kwargs):
            Calculate friction coefficient of the blade.
        CD_ow(*args, **kwargs):
            Calculate drag coefficient of the blade in open-water condition.
        CD_sp(*args, **kwargs):
            Calculate drag coefficient of the blade in self-propulsion condition.
        CD_fs(*args, **kwargs):
            Calculate drag coefficient of the blade in full-scale condition.
        CD(condition, RN, XE, DP, JEI=None, **kwargs):
            Calculate drag coefficient of the blade.
        initialize(propeller, openwater,
                   (viscosity_ow, viscosity_sp, viscosity_fs),
                   (density_ow, density_sp, density_fs),
                   (friction_ow, friction_sp, friction_fs),
                   formfactor)
            Initialize the scaling method.
        scale():
            Scale the open-water data.

    A call to the `IScalingPlugin.scale()` method sets the properties and
    methods listed below from the appropriate places, so that `_scale()` can
    use these values:
        SC:
            The model scale.
        DP[condition]:
            A dictionary with the propeller diameters for 'ow', 'sp' and 'fs'.
        N[condition]:
            A dictionary with the propeller shaft revs for 'ow', 'sp' and 'fs'.
            Note that `N['ow']` is not a value, but the function N(J), which
            must be evaluated at `JEI` to get the shaft revs.
        TEWA[condition]:
            A dictionary with the water temperatures for 'ow', 'sp' and 'fs'.
        scale_to[condition]:
            A dictionary if the open-water characterstics should be scaled to
            'sp' and 'fs'.
        cd_integrate:
            If the friction line should be integrated over the blade.
        N_ow(JEI):
            Calculate the propeller shaft revs N(J) in open-water condition.

    The following methods are calculated by the scaling method. These should
    be overwritten by the `scale()` method (see `Dummy(IScalingPlugin)`):
        KT_ow(JEI):
            Calculate the thrust coefficient KT(J) in open-water condition.
        KQ_ow(JEI):
            Calculate the torque coefficient KQ(J) in open-water condition.
        KT_sp(JEI):
            Calculate the thrust coefficient KT(J) in self-propulsion condition.
        KQ_sp(JEI):
            Calculate the torque coefficient KQ(J) in self-propulsion condition.
        KT_fs(JEI):
            Calculate the thrust coefficient KT(J) in full scale condition.
        KQ_fs(JEI):
            Calculate the torque coefficient KQ(J) in full scale condition.

    External programs
    =================

    If the scaling method writes it's own output file (which it should not do,
    but if the plug-in calls an external program, this is highly likely), the
    scaling method plug-in must return a dictionary with these output files.
    For the format see `_scale()`. The output plug-in will rename the file(s)
    to the name(s) given on the command line.

    Example as implemented by `owscaling.py`:
        1. The external program writes two files with the names `'latest.csv'`
           and `'latest.ypp'`. These files correspond to the output writer
           plugins `HSVA.csv` and `HSVA.ypp`.
        2. The scaling plug-in, which called the external program, stores this
           information in `self.files = {'HSVA.csv': 'latest.csv',
           'HSVA.ypp': 'latest.ypp'}`.
        3. The command line specifies the ouput with `--output HSVA.ypp
           scaled.ypp`.
        4. The `HSVA.ypp` renames the file `latest.ypp` to `scaled.ypp`.
        5. All remaining files (i.e. `'latest.csv'`) are deleted.
    """
    interfacename = 'scaling method'

    @autoargs
    def arguments(
            self,
            scale_to_sp: {
                'args': '--no-selfpropulsion',
                'action': 'store_false',
                'help': 'do not scale down to the condition '
                        'of the self-propulsion test'
                } = True,
            scale_to_fs: {
                'args': '--no-fullscale',
                'action': 'store_false',
                'help': 'do not scale up to the full-scale condition'
                } = True,
            XE: {
                'args': '--xe',
                'type': helpers.LimitedFloat(min=0, max=1, endpoints=']]'),
                'help': 'fractional radius of equivalent profile'
                } = 0.75,
            integrate: {
                'args': ('-i', '--integrate'),
                'action': 'store_true',
                'help': 'integrate the section drag values '
                        'over the whole blade'
                } = False,
            *args, **kwargs):
        super().arguments(*args, **kwargs)


    def __call__(self, *args, **kwargs):
        raise NotImplementedError(
                'To scale data with plug-in "{}" (class: {}) '
                'use the "scale()" method.'.format(
                        self.name, self.__class__.__name__))

    def _getattr(self, attr, condition):
        """Get the attribute `attr+'_'+condition`.

        Parameters:
            condition:
                The condition `'ow'`, `'sp'` or `'fs'` for "open-water",
                "self-propulsion" or "full scale".

        Return:
            The requested attribute for the selected `condition`.

        Exceptions:
            ValueError:
                Raised whenever the condition is not known.
        """
        try:
            return getattr(self, attr+'_'+condition)
        except AttributeError:
            raise ValueError('Condition "{}" unknown.'.format(condition))


    def KT(self, JEI, condition):
        """Calculate the thrust coefficient KT(J) for `condition`.

        A faster way is to call `KT_ow(JEI)`, `KT_sp(JEI)` and `KT_fs(JEI)`.

        Parameters:
            JEI:
                The J value where the KT curve should be calculated.
            condition:
                The condition `'ow'`, `'sp'` or `'fs'` for "open-water",
                "self-propulsion" or "full scale".

        Return:
            The KT value at `JEI` for the selected `condition`.
        """
        return self._getattr('KT', condition)(JEI)

    def KQ(self, JEI, condition):
        """Calculate the torque coefficient KQ(J) for `condition`.

        A faster way is to call `KQ_ow(JEI)`, `KQ_sp(JEI)` and `KQ_fs(JEI)`.

        Parameters:
            JEI:
                The J value where the KQ curve should be calculated.
            condition:
                The condition `'ow'`, `'sp'` or `'fs'` for "open-water",
                "self-propulsion" or "full scale".

        Return:
            The KQ value at `JEI` for the selected `condition`.
        """
        return self._getattr('KQ', condition)(JEI)

    def ETA(self, JEI, condition):
        """Calculate the efficiency η(J) for `condition`.

        Parameters:
            JEI:
                The J value where the η curve should be calculated.
            condition:
                The condition `'ow'`, `'sp'` or `'fs'` for "open-water",
                "self-propulsion" or "full scale".

        Return:
            The η value at `JEI` for the selected `condition`.
        """
        return JEI/(2*pi) * self.KT(JEI, condition) / self.KQ(JEI, condition)


    def KT_ow(self, JEI):
        """Calculate the thrust coefficient KT(J) in open-water condition."""
        raise NotImplementedError(
                '"scale()" must be called before "KT_ow" becomes available.')

    def KQ_ow(self, JEI):
        """Calculate the torque coefficient KQ(J) in open-water condition."""
        raise NotImplementedError(
                '"scale()" must be called before "KQ_ow" becomes available.')

    def KT_sp(self, JEI):
        """Calculate the thrust coefficient KT(J) in self-propulsion condition."""
        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must implement the "KT_sp" method and '
                '"scale()" must be called '
                'before "KT_sp" becomes available.'.format(
                        self.name, self.__class__.__name__))

    def KQ_sp(self, JEI):
        """Calculate the torque coefficient KQ(J) in self-propulsion condition."""
        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must implement the "KT_sp" method and '
                '"scale()" must be called '
                'before "KQ_sp" becomes available.'.format(
                        self.name, self.__class__.__name__))

    def KT_fs(self, JEI):
        """Calculate the thrust coefficient KT(J) in full scale condition."""
        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must implement the "KT_fs" method and '
                '"scale()" must be called '
                'before "KT_fs" becomes available.'.format(
                        self.name, self.__class__.__name__))

    def KQ_fs(self, JEI):
        """Calculate the torque coefficient KQ(J) in full scale condition."""
        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must implement the "KT_fs" method and '
                '"scale()" must be called '
                'before "KQ_fs" becomes available.'.format(
                        self.name, self.__class__.__name__))

    def CF_ow(self, *args, **kwargs):
        """Calculate friction coefficient of the blade in open-water condition."""
        return self.CF('ow', *args, **kwargs)

    def CF_sp(self, *args, **kwargs):
        """Calculate friction coefficient of the blade in self-propulsion condition."""
        return self.CF('sp', *args, **kwargs)

    def CF_fs(self, *args, **kwargs):
        """Calculate friction coefficient of the blade in full-scale condition."""
        return self.CF('fs', *args, **kwargs)

    def CF(self, condition, RN, XE, DP, JEI=None, **kwargs):
        """Calculate friction coefficient of the blade.

        Parameters:
            condition:
                The condition `'ow'`, `'sp'` or `'fs'` for "open-water",
                "self-propulsion" or "full scale".
            RN:
                The Reynolds number Rnₑ at the significant/equivalent profile
                at `XE`.
            XE:
                The fractional radius rₑ/R of the significant/equivalent
                profile.
            DP:
                The propeller diameter .
            JEI=None:
                The value of the advance coefficient J.
            **kwargs:
                Keyword arguments passed on to `CF()`.

        Returns:
            The friction coefficient of either the significant/equivalent
            profile at `XE` or integrated over the whole blade (if
            `self.cd_integrate` is `True`).
        """

        # Propeller radius
        RP = DP/2

        # Friction coefficient of profile
        try:
            CF = self.friction[condition]
        except AttributeError:
            raise ValueError('Unknown condition "{}"'.format(condition))

        # Chord length to radius distribution
        chr = self.propeller.chr
        # Chord length to radius of significant profile
        chr_xe = chr(XE)

        if self.cd_integrate:

            # Boss to diameter ratio
            XBDR = self.propeller.XBDR

            # Note:
            # We don't have the Reynolds number at x=r/R, so we have to
            # calculate it from the Reynolds number RN at the equivalent
            # profile:
            #   Rnₑ = vₑ·cₑ/ν
            #   Rnₓ = vₓ·cₓ/ν
            #       = vₓ·cₓ/ν · vₑ/vₑ·cₑ/cₑ = Rnₑ · vₓ/vₑ · cₓ/cₑ
            # With
            #   v = n·D·√[J²+(πx)²]
            #   vₓ/vₑ = √[J²+(πx)²]/[J²+(πxₑ)²]
            RNx = lambda x, RN, JEI, XE: (
                    RN *
                    chr(x) / chr_xe *
                    sqrt((JEI**2 + (pi*x)**2) / (JEI**2 + (pi*XE)**2)))

            # Drag coefficient of one side of section at x=r/R
            CFx = lambda x, RN, JEI, XE: (
                    CF(RNx(x, RN, JEI, XE), LCH_x=chr(x)*RP, **kwargs))

            return scipy.integrate.quad(
                    lambda x, RN, JEI, XE, kwargs: (
                            CFx(x, RN, JEI, XE) *
                            # Weighting factor!
                            (JEI**2 + (pi*x)**2) * chr(x)),
                    XBDR, 1, args=(RN, JEI, XE, kwargs))[0]

        else:

            return CF(RN, LCH_x=chr_xe*RP, **kwargs)

    def CD_ow(self, *args, **kwargs):
        """Calculate drag coefficient of the blade in open-water condition."""
        return self.CD('ow', *args, **kwargs)

    def CD_sp(self, *args, **kwargs):
        """Calculate drag coefficient of the blade in self-propulsion condition."""
        return self.CD('sp', *args, **kwargs)

    def CD_fs(self, *args, **kwargs):
        """Calculate drag coefficient of the blade in full-scale condition."""
        return self.CD('fs', *args, **kwargs)

    def CD(self, condition, RN, XE, DP, JEI=None, **kwargs):
        """Calculate drag coefficient of the blade.

        Parameters:
            condition:
                The condition `'ow'`, `'sp'` or `'fs'` for "open-water",
                "self-propulsion" or "full scale".
            RN:
                The Reynolds number Rnₑ at the significant/equivalent profile
                at `XE`.
            XE:
                The fractional radius rₑ/R of the significant/equivalent
                profile.
            DP:
                The propeller diameter .
            JEI=None:
                The value of the advance coefficient J.
            **kwargs:
                Keyword arguments passed on to `CD_2d()` and `CF()`.

        Returns:
            The drag coefficient of either the profile at the fractional radius
            `XE` or integrated over the whole blade (if `self.cd_integrate`
            is `True`).
        """

        # Propeller radius
        RP = DP/2

        # Formfactor of profile
        CD_2d = self.formfactor

        # Friction coefficient of profile
        try:
            CF = self.friction[condition]
        except AttributeError:
            raise ValueError('Unknown condition "{}"'.format(condition))

        # Chord length to radius distribution
        chr = self.propeller.chr
        # Chord length to radius of significant profile
        chr_xe = chr(XE)

        # Maximum thickness to chord length distribution
        xtc = self.propeller.xtc

        if self.cd_integrate:

            # Boss to diameter ratio
            XBDR = self.propeller.XBDR

            # Note:
            # We don't have the Reynolds number at x=r/R, so we have to
            # calculate it from the Reynolds number RN at the equivalent
            # profile:
            #   Rnₑ = vₑ·cₑ/ν
            #   Rnₓ = vₓ·cₓ/ν
            #       = vₓ·cₓ/ν · vₑ/vₑ·cₑ/cₑ = Rnₑ · vₓ/vₑ · cₓ/cₑ
            # With
            #   v = n·D·√[J²+(πx)²]
            #   vₓ/vₑ = √[J²+(πx)²]/[J²+(πxₑ)²]
            RNx = lambda x, RN, JEI, XE: (
                    RN *
                    chr(x) / chr_xe *
                    sqrt((JEI**2 + (pi*x)**2) / (JEI**2 + (pi*XE)**2)))

            # Drag coefficient of one side of section at x=r/R
            CDx = lambda x, RN, JEI, XE: (
                    CD_2d(xtc(x), **kwargs) *
                    CF(RNx(x, RN, JEI, XE), LCH_x=chr(x)*RP, **kwargs))

            return 2 * scipy.integrate.quad(
                    lambda x, RN, JEI, XE, kwargs: (
                            CDx(x, RN, JEI, XE) *
                            # Weighting factor!
                            (JEI**2 + (pi*x)**2) * chr(x)),
                    XBDR, 1, args=(RN, JEI, XE, kwargs))[0]

        else:

            return (2 *
                    CD_2d(xtc(XE), **kwargs) *
                    CF(RN, LCH_x=chr_xe*RP, **kwargs))

    def initialize(self,
                   propeller, openwater,
                   viscosities, densities, frictions, formfactor):
        """Initialize the scaling method.

        Parameters:
            propeller:
                The propeller geometry (one object implementing
                `IPropellergeometryPlugin`).
            openwater:
                The (measured) open-water curve (one object implementing
                `IOWReaderPlugin`).
            viscosities:
                Iterable with the viscosities of the fluid for open-water,
                self-propulsion and full scale conditions (three objects
                implementing `IViscosityPlugin`).
            densities:
                Iterable with the densities of the fluid for open-water,
                self-propulsion and full scale conditions (three objects
                implementing `IDensityPlugin`).
            frictions:
                Iterable with the friction lines for open-water,
                self-propulsion and full scale conditions (three objects
                implementing `IFrictionlinePlugin`).
            formfactor:
                The form factor of the profile (one object implementing
                `IFormfactorPlugin`).
        """

        self.propeller = propeller
        self.openwater = openwater

        self.viscosity = {'ow': viscosities[0],
                          'sp': viscosities[1],
                          'fs': viscosities[2]}
        self.density = {'ow': densities[0],
                        'sp': densities[1],
                        'fs': densities[2]}
        self.friction = {'ow': frictions[0],
                         'sp': frictions[1],
                         'fs': frictions[2]}
        self.formfactor = formfactor

    def scale(self, SC, N, TEWA):
        """Scale the open-water data.

        Parameters:
            SC:
                The model scale λ.
            N:
                A tuple with the propeller revolutions of the self-propulsion
                test and in full scale [Hz], e.g. (N_sp, N_fs).
            TEWA:
                A tuple with the water temperature of the self-propulsion test
                and in full scale [°C], e.g. (TEWA_sp, TEWA_fs).

        Return:
            A dictionary of output files as returned by `_scale()`.
        """

        # model scale
        self.SC = SC

        # Propeller shaft revs
        N_ow = self.openwater.N
        self.N = {'ow': N_ow, 'sp': N[0], 'fs': N[1]}

        # Water temperature
        self.TEWA = {'ow': self.openwater.TEWA, 'sp': TEWA[0], 'fs': TEWA[1]}

        # Propeller diameters
        DP_ow = self.openwater.DP
        self.DP = {'ow': DP_ow, 'sp': DP_ow, 'fs': DP_ow*SC}

        # Scaling regimes
        self.scale_to = {'sp': self.args.scale_to_sp,
                         'fs': self.args.scale_to_fs}

        # If the friction line should be integrated over the blade
        self.cd_integrate = self.args.integrate

        # Set the open-water characteristics, so that _scale() does not have
        # to do it
        self.KT_ow = self.openwater.KT
        self.KQ_ow = self.openwater.KQ

        # Finally scale the open-water data. Return the dictionary with the
        # output files or an empty dictionary.
        return self._scale() or {}

    def _scale(self):
        """Do the actual scaling of the open-water data.

        If the scaling method writes it's own output file (which it should
        not do, but if the plug-in calls an external program, this is highly
        likely), this method must return these output files as a dictionary.
        The keys are the names the corresponding plug-in producing the same
        output (the first argument to the `--output` option - basically the
        plug-in's name). The value of each key is the name of the output
        file. The output plug-in will rename the file to the name given on
        the command line.

        Overwrite it in your plug-in!

        Return:
            A dictionary of output files.
        """

        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must implement the "_scale()" method.'.format(
                        self.name, self.__class__.__name__))


class IPropellergeometryPlugin(_IFilereader,
                               metaclass=pluginframework.PluginMount):
    """Interface for plug-ins implementing a reader for propeller geometry.

    For hints how to derive from this factory class, see also the parent class
    `_IFilereader`.

    For simple scaling methods, it must implement two additional methods being
    able to calculate the required value at every fractional radius:
        1. `chr(X)`, the radial chord length distribution c(x)/R and
        2. `pdr(X)`, the radial pitch to diameter distribution P(x)/D.
    The method `fit()` might help.

    For more complex geometries, the following methods, being able to calculate
    the required value at every fractional radius, should be implemented:
        3. `xtc(X)`, the radial thickness distribution tₘₐₓ(x)/c(x),
        4. `f(X)`, the radial camber distribution fₘₐₓ(x)/c(x) and
        5. `rkr(X)`, the radial rake distribution rk(x)/R.

    It must also add the following property:
        1. `NPB`, the number of propeller blades Z
    and should add these properties:
        2. `PDR`, the mean pitch to diameter ratio Pₘ/D,
        3. `ADE`, the blade area ratio A₀/Aₑ,
        4. `XBDR`, the hub diameter ratio Dₕ/D and
        5. `hand`, the hand (`0` for right, `1` for left).

    The obvious place to do this, is the to be implemented `_read()` method.

    Required properties:
        NPB:
            The number of propeller blades Z.

    Recommended properties:
        PDR:
            The mean pitch to diameter ratio Pₘ/D.
        ADE:
            The blade area ratio A₀/Aₑ.
        XBDR:
            The hub diameter ratio Dₕ/D.
        hand:
            The hand (`0` for right, `1` for left).

    Required methods:
        chr(X):
            Calculate the chord length c(r/R)/R.
        pdr(X):
            Calculate the pitch to diameter P(r/R)/D.

    Recommended methods:
        xtc(X):
            Calculate the thickness tₘₐₓ(r/R)/c(r/R).
        f(X):
            Calculate the camber fₘₐₓ(r/R)/c(r/R).
        rkr(X):
            Calculate the rake rk(r/R)/R.
    """
    interfacename = 'propeller geometry file reader'

    @autoargs
    def arguments(
            self,
            file: {
                'type': argparse.FileType('r'),
                'help': 'file with propeller geometry',
                'metavar': 'FILE'
                },
            *args, **kwargs):
        super().arguments(*args, **kwargs)


    def chr(self, XE):
        """Calculate the chord length c(r/R)/R [-]."""
        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must implement the "chr" method.'.format(
                        self.name, self.__class__.__name__))

    def pdr(self, XE):
        """Calculate the pitch to diameter P(r/R)/D [-]."""
        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must implement the "pdr" method.'.format(
                        self.name, self.__class__.__name__))

    def fit(self, X, chr, pdr, xtc=None, f=None, rkr=None, skew=None):
        """Fit and store polynomial through c/R and PITCH/D distribution.

        A helper function, because this is needed by most plug-ins reading
        propeller geometry data. It stores the fitted curves in `self.chr`
        and `self.pdr`.

        Parameters:
            X:
                The iterable with the fractional radii.
            chr:
                The iterable with the chord length distribution c(X)/R.
            pdr:
                The iterable with the pitch to diameter distribution
                PITCH(X)/D.
            xtc=None:
                The iterable with the thickness distribution TMAX(X)/c(X).
            f=None:
                The iterable with the camber distribution f(X)/c(X).
            rkr=None:
                The iterable with the rake distribution rk(X)/R.
            skew=None:
                The iterable with the skew distribution skew(X)/R (=Distance
                of midchord point to generator line).

        Class properties added:
            chr:
                Curve fitted to c/R distribution.
            pdr:
                Curve fitted to PITCH/D distribution.
        """

        # We cannot use "strict=True" here, because more often than not the
        # hub diameter is smaller than the lowest section.

        #: Chord length distribution c(r/R)/R [-]
        self.chr = helpers.ApproxFit(X, chr, strict=False)
        #: Pitch to diameter distribution p(r/R)/D [-]
        self.pdr = helpers.ApproxFit(X, pdr, strict=False)
        if xtc:
            #: Thickness distribution data tmax[r/R]/R [-]
            self.xtc = helpers.ApproxFit(X, xtc, strict=False)
        if f:
            #: Camber distribution data f[r/R]/c(x) [-]
            self.f = helpers.ApproxFit(X, f, strict=False)
        if rkr:
            #: Rake distribution data rk[r/R]/R [-]
            self.rkr = helpers.ApproxFit(X, rkr, strict=False)
        if skew:
            #: Distance of midchord point to generator line skew[r/R]/R [-]
            self.skew = helpers.ApproxFit(X, skew, strict=False)


class IOWReaderPlugin(_IFilereader, metaclass=pluginframework.PluginMount):
    """Interface for plug-ins implementing a reader for popeller open-water data.

    For hints how to derive from this factory class, see also the parent class
    `_IFilereader`.

    It must store three additional methods being able to calculate the
    required value at every J:
        1. `N(JEI)`, the (continious) shaft revs N(J),
        2. `KT(JEI)`, the (continuous) thrust coefficient KT(J) and
        3. `KQ(JEI)`, the (continuous) torque coefficient KQ(J).

    It also must store the following values as properties:
        1. `DP`, the propeller diameter,
        2. `TEWA`, the water temperature and
        3. `JEI_data`, the list of discrete JEI values.

    The obvious place to do this, is in the to be implemented `_read()`
    method. The method `fit()` might help.

    Required properties:
        DP:
            The propeller diameter.
        TEWA:
            The water temperature.
        JEI_data:
            The list of discrete JEI values.

    Required methods:
        N(JEI):
            Calculate propeller shaft revs N(J).
        KT(JEI):
            Calculate the thrust coefficient KT(J).
        KQ(JEI):
            Calculate the torque coefficient KQ(J).
    """
    interfacename = 'open-water data file reader'

    @autoargs
    def arguments(
            self,
            file: {
                'type': argparse.FileType('r'),
                'help': 'file with open-water test data',
                'metavar': 'FILE'
                },
            *args, **kwargs):
        super().arguments(*args, **kwargs)


    def N(self, JEI):
        """Calculate propeller shaft revs N(J) [Hz]."""
        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must store the "N" method.'.format(
                        self.name, self.__class__.__name__))

    def KT(self, JEI):
        """Calculate the thrust coefficient KT(J) [-]."""
        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must store the "KT" method.'.format(
                        self.name, self.__class__.__name__))

    def KQ(self, JEI):
        """Calculate the torque coefficient KQ(J) [-]."""
        raise NotImplementedError(
                'Plug-in "{}" (class: {}) '
                'must store the "KQ" method.'.format(
                        self.name, self.__class__.__name__))

    def fit(self, JEI, KT, KQ, N):
        """Fit and store polynomials through KT, KQ and N data.

        A helper function, because this is needed by most plug-ins reading
        open-water test data. It stores the polynomials fitted to KT, KQ and
        N in `self.KT`, `self.KQ` and `self.N`.

        Parameters:
            JEI:
                The iterable with the advance coefficient J.
            KT:
                The iterable with the thrust coefficient data KT(J).
            KQ:
                The iterable with the torque coefficient data KQ(J).
            N:
                The iterable with the shaft revolution data N(J). This can be
                a single numerical value.
        """

        #: Thrust coefficients KT(J) [-]
        self.KT = helpers.PolyFit(JEI, KT)
        #: Torque coefficients KQ(J) [-]
        self.KQ = helpers.PolyFit(JEI, KQ)
        #: Shaft revolutions N(J) [Hz]
        if isinstance(N, Number):
            self.N = lambda JEI: N
        else:
            self.N = helpers.PolyFit(JEI, N)


class IOWWriterPlugin(_IFilewriter, metaclass=pluginframework.PluginMount):
    """Interface for plug-ins implementing a file writer for open-water data.

    For hints how to derive from this factory class, see also the parent class
    `_IFilewriter`.

    Methods:
        write():
            Write data to the output file `self.args.file`.
        move(outputfilename):
            Rename `outputfilename` to the file name of the command line.
    """
    interfacename = 'open-water data writer'

    @autoargs
    def arguments(
            self,
            file: {
                'type': argparse.FileType('w'),
                'help': 'name of output file',
                'metavar': 'FILE'
                },
            *args, **kwargs):

        super().arguments(*args, **kwargs)

        # Close output file now, because it can happen that an external method
        # uses a file of the same name (this is only a problem in Windows...)
        f = self.args.file
        try:
            f.close()
        except AttributeError:
            pass


    def write(self, scaling):
        """Write data to the output file `self.args.file`.

        This overrides the `_IFilewriter.write()` method, because we have to
        re-open the output file (which was closed during class initialization).

        Parameters:
            scaling:
                An object of the type `IScalingPlugin` supplying the scaled
                open-water data.
        """

        # Store the scaling method for later use
        self.scaling = scaling

        # Write to the file.
        if isinstance(self.args.file, str):     # Check if we got a string
            with open(self.args.file, 'w') as f:
                self._write(f)
        else:
            # Re-open the file we closed in `__init__()`
            with open(self.args.file.name, 'w') as f:
                self._write(f)

    def move(self, outputfilename):
        """Rename `outputfilename` to the file name of the command line.

        Some scaling methods write their output files independent of this
        framework. This can happen whenever external programs are called. This
        method renames the `outputfilename` written by the scaling method to
        the file name supplied at the command line.

        Parameters:
            outputfilename:
                The name of the file to be renamed to the file name supplied
                at the command line.
        """

        # Get the name of the output file corresponding to the plug-in
        filename = self.get_filename()

        # Rename file, but only if names are different
        if outputfilename != filename:
            # Windows insists to have the target file removed first (shrug)
            os.remove(filename)
            os.rename(outputfilename, filename)
