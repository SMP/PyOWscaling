#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A dummy external program which creates two dummy output files.

This is used when testing scaling methods implemented through external
programs.


Created on Tue Feb 21 14:36:40 2017


Copyright (C) 2017  Stephan Helma
Copyright (C) 2017  Stone Marine Propulsion Ltd

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__version__ = '3.1.0'


import shutil
import time


FILES = ('latest.csv', 'latest.ypp')


for file in FILES:
    with open(file, 'w') as f:
        print(f.name, file=f)
        print(time.asctime(), file=f)

shutil.copy2('docs/latest.ypp', './latest.ypp')
